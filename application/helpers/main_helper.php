<?php

function dd($message, $is_exit = '')
{
    switch ($is_exit) {
        case 1:
            echo "<pre>";
            var_dump($message);
            echo "</pre>";
            break;

        default:
            echo "<pre>";
            var_dump($message);
            echo "</pre>";
            die();
            break;
    }
}
function getUsulan($id)
{
    if ($id == 'dvt') {
        $usul = 'Pengabdian';
    } else if ($id == 'rsc') {
        $usul = 'Penelitian';
    } else {
        $usul = '-';
    }

    return $usul;
}
function getTblusulan($usulan)
{
    if ($usulan == 'dvt') {
        $table = 'tbl_pengabdian';
    } elseif ($usulan == 'rsc') {
        $table = 'tbl_penelitian';
    } else {
        $table = false;
    }

    return $table;
}
function getJabfung($id)
{
    if ($id == 'TPD') {
        $jabfung = 'Tenaga Pengajar';
    } else if ($id == 'SAH') {
        $jabfung = 'Asisten Ahli';
    } else if ($id == 'LKT') {
        $jabfung = 'Lektor';
    } else if ($id == 'LKK') {
        $jabfung = 'Lektor Kepala';
    } else if ($id == 'BIG') {
        $jabfung = 'Guru Besar';
    } else {
        $jabfung = '-';
    }

    return $jabfung;
}
function getJenis($id)
{
    if ($id == 'int') {
        $jenis = 'Internal';
    } else if ($id == 'mdr') {
        $jenis = 'Mandiri';
    } else if ($id == 'hbh') {
        $jenis = 'Hibah';
    } else if ($id == 'csr') {
        $jenis = 'CSR';
    } else if ($id == 'spr') {
        $jenis = 'Sponsor';
    } else {
        $jenis = '-';
    }

    return $jenis;
}
function getTahun()
{
    $http_header = ['x-lppmp-key: ' . APP_KEY, 'Content-Type: application/json'];
    $hostapi = HOST_ENDPOINT;
    $CI = &get_instance();
    $data = $CI->curl_lib->exec_curl($hostapi . '/api_lppmp/active_year', $http_header);
    $decode_data = json_decode($data);
    return $decode_data->data->nama_tahun;
}
function getStatus($id)
{
    if ($id == 99) {
        $q = '<b>Menunggu persetujuan setiap anggota</b>';
    } elseif ($id == 0) {
        $q = '<b>Menunggu disetujui oleh fakultas</b>';
    } elseif ($id == 1) {
        $q = '<b style="color:green">Disetujui Fakultas,Menunggu hasil Review LPPMP</b>';
    } elseif ($id == 2) {
        $q = '<b style="color:#ffa500">Revisi</b>';
    } elseif ($id == 11) {
        $q = '<b style="color:green">Disetujui LPPMP, Menunggu persetujuan Rektor</b>';
    } elseif ($id == 12) {
        $q = '<b style="color:#ffa500">Revisi</b>';
    } elseif ($id == 13) {
        $q = '<b style="color:red">Ditolak LPPMP</b>';
    } elseif ($id == 21) {
        $q = '<b style="color:green">Disetujui Rektor, Dana sedang di proses BPAK</b>';
    } elseif ($id == 31) {
        $q = '<b style="color:green">Dana telah dibayar 50%</b>';
    } elseif ($id == 32) {
        $q = '<b style="color:green">Dana telah dibayar 100%</b>';
    } else {
        $q = '-';
    }

    return $q;
}

function getFakultasname($id)
{
    $http_header = ['x-lppmp-key: ' . APP_KEY, 'Content-Type: application/json'];
    $hostapi = HOST_ENDPOINT;
    $CI = &get_instance();
    $data = $CI->curl_lib->exec_curl($hostapi . '/api_lppmp/fakultas', $http_header, 1, $id);
    $decode_data = json_decode($data);
    return $decode_data->data;
}
function getFakultas()
{
    $http_header = ['x-lppmp-key: ' . APP_KEY, 'Content-Type: application/json'];
    $hostapi = HOST_ENDPOINT;
    $CI = &get_instance();
    $data = $CI->curl_lib->exec_curl($hostapi . '/api_lppmp/allFakultas', $http_header);
    $decode_data = json_decode($data);
    return $decode_data->data;
}
function getDsn_autocomplete($id)
{
    $http_header = ['x-lppmp-key: ' . APP_KEY, 'Content-Type: application/json'];
    $hostapi = HOST_ENDPOINT;
    $CI = &get_instance();
    $data = $CI->curl_lib->exec_curl($hostapi . '/api_lppmp/dosen', $http_header, 1, $id);
    $decode_data = json_decode($data);
    return $decode_data->data;
}
function time_ago($datetime)
{
    $datediff = abs(strtotime(date("Y-m-d H:i:s")) - strtotime($datetime));
    $difftext = "";
    $years = floor($datediff / (365 * 60 * 60 * 24));
    $months = floor(($datediff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
    $days = floor(($datediff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
    $hours = floor($datediff / 3600);
    $mnt = floor($datediff % 3600 / 60);
    $minutes = floor($datediff / 60);
    $dtk = floor($datediff % 60);
    $seconds = floor($datediff);

    //year checker
    if ($difftext == "") {
        if ($years >= 1)
            $difftext = $years . " tahun yang lalu";
    }
    //month checker
    if ($difftext == "") {
        if ($months >= 1)
            $difftext = $months . " bulan yang lalu";
    }
    //day checker
    if ($difftext == "") {
        if ($days >= 1)
            $difftext = $days . " hari yang lalu";
    }
    //hour checker
    if ($difftext == "") {
        if ($hours >= 1)
            $difftext = $hours . " jam yang lalu";
    }
    //minutes checker
    if ($difftext == "") {
        if ($minutes >= 1)
            $difftext = $minutes . " menit yang lalu";
    }
    //seconds checker
    if ($difftext == "") {
        if ($seconds >= 1)
            $difftext = "baru saja";
    }
    return $difftext;
}
function getidbykdusulan($kd,$usulan)
{
    $CI = &get_instance();

    $CI->db->where('kd_usulan', $kd);
    $q = $CI->db->get(getTblusulan($usulan))->row();

    return $q->id;
}
function getNamedosen($id)
{
    $http_header = ['x-lppmp-key: ' . APP_KEY, 'Content-Type: application/json'];
    $hostapi = HOST_ENDPOINT;
    $CI = &get_instance();
    $data = $CI->curl_lib->exec_curl($hostapi . '/api_lppmp/nmdosen', $http_header, 1, $id);
    $decode_data = json_decode($data);
    return $decode_data->data;
}
// lsp

function getImage($id)
{
    $CI = &get_instance();

    $CI->db->where('id', $id);
    $q = $CI->db->get('tbl_apl01_berkas')->row();

    return $q->path . '/' . $q->berkas;
}
function checkImage($id)
{
    $CI = &get_instance();

    $CI->db->where('id', $id);
    $q = $CI->db->get('tbl_apl01_berkas')->row();

    return $q->is_deleted;
}
function getNameimg($id)
{
    $CI = &get_instance();

    $CI->db->where('id', $id);
    $q = $CI->db->get('tbl_apl01_berkas')->row();

    return $q->berkas;
}
function getSyarat($id)
{
    $CI = &get_instance();

    $CI->db->where('id', $id);
    $q = $CI->db->get('tbl_syarat_dasar')->row();

    return $q->syarat;
}
function getUnit($id)
{
    $CI = &get_instance();

    $CI->db->where('id', $id);
    $q = $CI->db->get('tbl_unit')->row();

    return $q->unit;
}
function getKdunit($id)
{
    $CI = &get_instance();

    $CI->db->where('id', $id);
    $q = $CI->db->get('tbl_unit')->row();

    return $q->kd_unit;
}
function getSkema($id)
{
    $CI = &get_instance();

    $CI->db->where('id', $id);
    $q = $CI->db->get('tbl_skema')->row();

    return $q->skema;
}
function getNoskema($id)
{
    $CI = &get_instance();

    $CI->db->where('id', $id);
    $q = $CI->db->get('tbl_skema')->row();

    return $q->no_skema;
}
function getTujuan($id)
{
    if ($id == 1) {
        $q = 'Sertifikasi';
    } elseif ($id == 2) {
        $q = 'Sertifikasi Ulang';
    } else {
        $q = '-';
    }

    return $q;
}
function getSkemastat($id)
{
    if ($id == 1) {
        $q = '<b style="color:green">Aktif</b>';
    } elseif ($id == 2) {
        $q = '<b style="color:red">Tidak Aktif</b>';
    } else {
        $q = '-';
    }

    return $q;
}
function getStatus2($id)
{
    if ($id == 0) {
        $q = 'Belum Disetujui';
    } elseif ($id == 1) {
        $q = 'Disetujui%2C Silahkan melakukan pembayaran Melalui BRIVA sebesar Rp 1.000.000';
    } elseif ($id == 2) {
        $q = 'Tidak Disetujui%2C Harap revisi APL 01 dan APL 02 kamu%2C lalu ajukan kembali';
    } else {
        $q = '-';
    }

    return $q;
}
function getJenkel($id)
{
    if ($id == 'L') {
        $q = 'Laki-Laki';
    } elseif ($id == 'P') {
        $q = 'Wanita';
    } else {
        $q = '-';
    }

    return $q;
}
function getProdi($id)
{
    $http_header = ['x-lsp-key: ' . APP_KEY, 'Content-Type: application/json'];
    $hostapi = HOST_ENDPOINT;
    $CI = &get_instance();
    $data = $CI->curl_lib->exec_curl($hostapi . '/api_lsp/prodi', $http_header, 1, $id);
    $decode_data = json_decode($data);
    return $decode_data->data;
}
function getallProdi()
{
    $http_header = ['x-lsp-key: ' . APP_KEY, 'Content-Type: application/json'];
    $hostapi = HOST_ENDPOINT;
    $CI = &get_instance();
    $data = $CI->curl_lib->exec_curl($hostapi . '/api_lsp/allProdi', $http_header);
    $decode_data = json_decode($data);
    return $decode_data->data;
}

function datetimeIdn($date)
{
    $BulanIndo = array("", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
    $split = explode(' ', $date);
    $spli = explode('-', $split[0]);
    return  $split[1] . ' / ' . $spli[2] . ' ' . $BulanIndo[(int) $spli[1]] . ' ' . $spli[0];
}
function datetimeNotime($date)
{
    $BulanIndo = array("", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
    $split = explode(' ', $date);
    $spli = explode('-', $split[0]);
    return  $spli[2] . ' ' . $BulanIndo[(int) $spli[1]] . ' ' . $spli[0];
}

function TanggalIndo($date, $format = '')
{
    $BulanIndo = array("", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");


    $split = explode('-', $date);

    if ($format == 'd M Y') {
        return $split[0] . ' ' . $BulanIndo[(int) $split[1]] . ' ' . $split[2];
    } else {
        return $split[2] . ' ' . $BulanIndo[(int) $split[1]] . ' ' . $split[0];
    }
}
