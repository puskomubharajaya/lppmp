<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_proposal extends CI_Model
{


    function getPropanggota($tbl, $nid)
    {
        $sql = $this->db->query('SELECT a.`id`, a.`kd_usulan`,a.`approval`,b.`nid`,b.`nama`,b.`judul`,b.`proposal`,b.`tahun`,b.`status`
                                    FROM
                                    tbl_anggota a
                                    JOIN ' . $tbl . ' b
                                        ON a.`kd_usulan` = b.`kd_usulan`
                                    WHERE a.nid = ' . $nid . '
                                    AND a.`is_deleted` = 0
                                    AND b.`is_deleted` = 0
                                    ORDER BY a. nid DESC');
        if ($sql->num_rows() > 0) {
            return $sql->result();
        } else {
            return array();
        }
    }

    function getNotdeal($kd)
    {
        $sql = $this->db->select('*')
        ->from('tbl_anggota')
        ->where('kd_usulan', $kd)
        ->where('is_deleted', 0)
        ->where('approval !=', 1)
        ->order_by('id', 'DESC')
        ->get();
        return $sql;
    }
}
