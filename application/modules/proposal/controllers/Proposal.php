<?php defined('BASEPATH') or exit('No direct script access allowed');

class Proposal extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_proposal');
        if (!$this->session->userdata('lppmp_session') || is_null($this->session->userdata('lppmp_session'))) {
            redirect('errors/error_404/alertlogin');
        }
        $this->datetime = date('YmdHis');
    }
    public function usulan($prop)
    {
        $data['prop']  = $prop;
        $data['page']  = 'v_proposal';
        $this->load->view('template/homepage', $data);
    }
    public function edit($id, $usulan)
    {
        $data['prop']     = $usulan;
        $data['proposal'] = $this->model_crud->getDetail(getTblusulan($usulan), 'id', $id, 'id', 'DESC')->row();
        $data['page']     = 'v_proposal_edit';
        $this->load->view('template/homepage', $data);
    }
    public function showanggota($id, $usulan)
    {
        $prop                = $this->model_crud->getDetail(getTblusulan($usulan), 'id', $id, 'id', 'DESC')->row();
        $data['listAnggota'] = $this->model_crud->getDetail('tbl_anggota', 'kd_usulan', $prop->kd_usulan, 'id', 'DESC');
        $data['notDeal']     = $this->model_proposal->getNotdeal($prop->kd_usulan);
        $this->load->view('modal_view_anggota', $data);
    }
    public function showstatus($kd)
    {
        $data['status_log']  = $this->model_crud->getDetail('tbl_status_log', 'kd_usulan', $kd, 'id', 'DESC')->result();
        $this->load->view('modal_view_status', $data);
    }
    public function myProposal($usulan)
    {
        $data['proposal'] = $usulan;
        $data['usulan']   = $this->model_crud->getDetail(getTblusulan($usulan), 'nid', $this->session->userdata('lppmp_session')['userid'], 'id', 'DESC')->result();
        $data['anggota']  = $this->model_proposal->getPropanggota(getTblusulan($usulan), $this->session->userdata('lppmp_session')['userid']);
        $data['page']     = 'v_myproposal';
        $this->load->view('template/homepage', $data);
    }
    public function submit()
    {

        // membuat kode unik setiap usulan
        $kd   = rawurlencode(base64_encode($this->input->post('nid') . '.' . $this->input->post('usulan') . '.' . $this->datetime));

        $loop = 0;
        for ($i = 0; $i < count($this->input->post('nid_anggota')); $i++) {
            if ($this->input->post('nid_anggota')[$i] != '') {
                $agt = array(
                    'jenis_usulan' => $this->input->post('usulan'),
                    'kd_usulan'    => $kd,
                    'anggota'      => 'anggota ' . ($i + 1),
                    'nid'          => $this->input->post('nid_anggota')[$i],
                    'nama'         => $this->input->post('nama_anggota')[$i],
                    'approval'     => 0,
                    "created_at"   => date('Y-m-d H:i:s'),
                    "created_by"   => $this->session->userdata('lppmp_session')['userid'],
                    "is_deleted"   => 0
                );
                // membuat notifikasi anggota yang di undang
                $this->_createNotif($kd, $this->input->post('nid_anggota')[$i], ' mengundang anda sebagai anggota');

                // simpan data anggota
                $this->model_crud->insertdata('tbl_anggota', $agt);
                $loop++;
            }
        };

        if ($loop == 0) {
            $status  = 0;
            $current = 'Usulan telah diajukan ke fakultas';
            $next    = 'Menunggu disetujui oleh fakultas';
            // membuat notifikasi ke fakultas
            $this->_createNotif($kd, $this->session->userdata('lppmp_session')['fakultas'], ' mengajukan usulan');
        } else {
            $status  = 99;
            $current = 'Usulan telah dibuat';
            $next    = 'Menunggu persetujuan setiap anggota';
        }

        $data = array(
            'kd_usulan'  => $kd,
            'nid'        => $this->input->post('nid'),
            'nidn'       => $this->input->post('nidn'),
            'nama'       => $this->input->post('nama'),
            'tlp'        => $this->input->post('tlp'),
            'email'      => $this->input->post('email'),
            'fakultas'   => $this->session->userdata('lppmp_session')['fakultas'],
            'judul'      => $this->input->post('judul'),
            'proposal'   => $this->input->post('proposal'),
            'plagiarism' => $this->input->post('plagiarism'),
            'jabfung'    => $this->input->post('jabfung'),
            'jenis'      => $this->input->post('jenis'),
            'dana'       => $this->input->post('dana'),
            'tahun'      => getTahun(),
            'status'     => $status,
            "created_at" => date('Y-m-d H:i:s'),
            "created_by" => $this->session->userdata('lppmp_session')['userid'],
            "is_deleted" => 0
        );
        // menyimpan riwayat status pengajuan
        $this->_createLogstatus($this->input->post('usulan'), $kd, $status, $current, $next);
        // menambah data usulan penelitian/pengabdian
        $this->model_crud->insertdata(getTblusulan($this->input->post('usulan')), $data);
        // redirect ke halaman usulan
        echo '<script>alert("Berhasil!");window.location.href="' . base_url('proposal/myProposal/' . $this->input->post('usulan')) . '";</script>';
    }

    public function update()
    {
        if ($this->input->post('status') == 2) {
            $status = 0;
            $to     = $this->session->userdata('lppmp_session')['fakultas'];
        } elseif ($this->input->post('status') == 12) {
            $status = 1;
            $to     = 'LPPMP';
        }
        $data = array(
            'tlp'        => $this->input->post('tlp'),
            'email'      => $this->input->post('email'),
            'judul'      => $this->input->post('judul'),
            'proposal'   => $this->input->post('proposal'),
            'plagiarism' => $this->input->post('plagiarism'),
            'status'     => $status,
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => $this->session->userdata('lppmp_session')['userid']
        );
        // membuat notifikasi ke fakultas atau lppmp
        $this->_createNotif($this->input->post('kd_usulan'), $to, ' telah melakukan revisi');
        // menyimpan riwayat status pengajuan
        $this->_createLogstatus($this->input->post('usulan'), $this->input->post('kd_usulan'), $status, 'Usulan telah direvisi', 'Menunggu disetujui oleh fakultas');
        // mengubah status pengajuan ke "mengajukan ke fakultas"
        $this->model_crud->updatedata(getTblusulan($this->input->post('usulan')), 'id', $this->input->post('id'), $data);
        // redirect ke halaman usulan
        echo '<script>alert("Berhasil!");window.location.href="' . base_url('proposal/myProposal/' . $this->input->post('usulan')) . '";</script>';
    }
    function load_dosen_autocomplete()
    {
        // autocomplete data dosen
        $data = getDsn_autocomplete($this->input->post('term', TRUE));
        echo ($data);
    }

    public function approve($id, $option)
    {
        $anggota = $this->model_crud->getdetail('tbl_anggota', 'id', $id, 'id', 'DESC')->row();
        $prop    = $this->model_crud->getdetail(getTblusulan($anggota->jenis_usulan), 'kd_usulan', $anggota->kd_usulan, 'id', 'DESC')->row();
        if ($option == 'yes') {
            $approval = 1;
            // notifikasi ke ketua usulan jika setuju
            $this->_createNotif($prop->kd_usulan, $prop->nid, ' setuju sebagai anggota anda');
        } elseif ($option == 'no') {
            $approval = 2;
            // notifikasi ke ketua usulan jika setuju
            $this->_createNotif($prop->kd_usulan, $prop->nid, ' menolak sebagai anggota anda');
        }
        $data = array(
            'approval'   => $approval,
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => $this->session->userdata('lppmp_session')['userid']
        );

        // menerima/menolak ajakan sebagai anggota
        $this->model_crud->updatedata('tbl_anggota', 'id', $id, $data);
        echo "<script>alert('Berhasil!');history.go(-1);</script>";
    }
    
    public function ajukan($id, $usulan)
    {
        $prop = $this->model_crud->getdetail(getTblusulan($usulan), 'id', $id, 'id', 'DESC')->row();

        // notifikasi ke fakultas kalau sudah mengajukan
        $this->_createNotif($prop->kd_usulan, $this->session->userdata('lppmp_session')['fakultas'], ' mengajukan usulan');
        // menyimpan riwayat status pengajuan
        $this->_createLogstatus($usulan, $prop->kd_usulan, 0, 'Usulan telah diajukan ke fakultas', 'Menunggu disetujui oleh fakultas');

        $data = array(
            'status'     => 0,
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => $this->session->userdata('lppmp_session')['userid']
        );

        // mengubah status pengajuan ke "mengajukan ke fakultas"
        $this->model_crud->updatedata(getTblusulan($usulan), 'id', $id, $data);
        echo "<script>alert('Berhasil!');history.go(-1);</script>";
    }

    public function ubahanggota()
    {
        for ($i = 0; $i < count($this->input->post('nid_anggota')); $i++) {
            if ($this->input->post('nid_anggota')[$i] != '') {
                $agt = array(
                    'nid'        => $this->input->post('nid_anggota')[$i],
                    'nama'       => $this->input->post('nama_anggota')[$i],
                    'approval'   => 0,
                    "updated_at" => date('Y-m-d H:i:s'),
                    "updated_by" => $this->session->userdata('lppmp_session')['userid']
                );
                // membuat notifikasi anggota yang di undang
                $this->_createNotif($this->input->post('kd_usulan'), $this->input->post('nid_anggota')[$i], ' mengundang anda sebagai anggota');
                // mengganti anggota
                $this->model_crud->updatedata('tbl_anggota', 'id', $this->input->post('id_anggota')[$i], $agt);
            }
        };
        echo "<script>alert('Berhasil!');history.go(-1);</script>";
    }

    private function _createNotif($kd, $to, $content)
    {
        $notif = array(
            'jns_notif'  => 1,
            'kd_usulan'  => $kd,
            'to'         => $to,
            'from'       => $this->session->userdata('lppmp_session')['userid'],
            'content'    => $this->session->userdata('lppmp_session')['name'] . $content,
            'is_read'    => 0,
            "created_at" => date('Y-m-d H:i:s'),
            "created_by" => $this->session->userdata('lppmp_session')['userid'],
            "is_deleted" => 0
        );
        // membuat notifikasi ke fakultas atau lppmp
        $this->model_crud->insertdata('tbl_notification', $notif);
    }
    private function _createLogstatus($usulan, $kd_usulan, $status, $current, $next)
    {
        $log = array(
            'jenis_usulan' => $usulan,
            'kd_usulan'    => $kd_usulan,
            'status'       => $status,
            "current_desc" => $current,
            "next_desc"    => $next,
            "created_at"   => date('Y-m-d H:i:s'),
            "created_by"   => $this->session->userdata('lppmp_session')['userid'],
            "is_deleted"   => 0
        );
        // menyimpan riwayat status pengajuan
        $this->model_crud->insertdata('tbl_status_log', $log);
    }
}
