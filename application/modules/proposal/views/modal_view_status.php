<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="gridSystemModalLabel">Riwayat status</h4>
</div>
<form class="form-horizontal" action="<?php echo base_url(); ?>proposal/ubahanggota" method="post">
    <div class="modal-body">
        <div style="text-align: center;">
            <table class="table table-hover">
                <?php $i=0; 
                foreach ($status_log as $key) { ?>
                    <tbody>
                        <tr <?php echo $i==0?'class="success"':''?>>
                            <td><?php echo $key->current_desc  ?></td>
                            <td><?php echo $key->next_desc ?></td>
                            <td><?php echo time_ago($key->created_at) ?></td>
                        </tr>
                    </tbody>
                <?php $i++;} ?>
            </table>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
</form>