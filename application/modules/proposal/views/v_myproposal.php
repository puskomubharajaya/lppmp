<script>
    function modalang(id, usulan) {
        $("#modalView").load('<?php echo base_url() ?>proposal/showanggota/' + id + '/' + usulan);
    }

    function log(kd) {
        $("#modalViewlog").load('<?php echo base_url() ?>proposal/showstatus/' + kd);
    }
</script>
<section id="inner-headline">
    <div class="container">
        <div class="row">
            <div class="span12">
                <div class="inner-heading">
                    <h2>Usulan <?php echo getUsulan($proposal) ?> Anda</h2>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="content">
    <div class="container">
        <div class="row">
            <div class="span12">
                <div class="tabbable tabs-top">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#ketuaTab" data-toggle="tab">Sebagai Ketua</a></li>
                        <li><a href="#anggotaTab" data-toggle="tab">Sebagai Anggota</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="ketuaTab">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Judul</th>
                                            <th>Status</th>
                                            <th>Catatan</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i = 1;
                                        foreach ($usulan as $key) { ?>
                                            <tr>
                                                <td>
                                                    <?php echo $i ?>
                                                </td>
                                                <td>
                                                    <a href="<?php echo $key->proposal ?>" target="_BLANK"><?php echo $key->judul ?></a>
                                                </td>
                                                <td>
                                                    <?php echo getStatus($key->status) ?>
                                                </td>
                                                <?php $status_log = $this->model_crud->getDetail('tbl_status_log', 'kd_usulan', $key->kd_usulan, 'id', 'DESC')->row(); ?>
                                                <td>
                                                    <?php echo $status_log->note ?>
                                                </td>
                                                <?php 
                                                // cek anggota dari usulan
                                                $listAnggota = $this->model_crud->getDetail('tbl_anggota', 'kd_usulan', $key->kd_usulan, 'id', 'DESC'); 
                                                ?>
                                                <td>
                                                    <a class="btn btn-orange" onclick="log('<?php echo $key->kd_usulan ?>')" data-toggle="modal" href="#modalLog" title="Riwayat status">
                                                        <i class="icon-exchange"></i>
                                                    </a>
                                                    <?php
                                                    // jika ada anggotanya 
                                                    if ($listAnggota->num_rows() > 0) {
                                                    ?>
                                                        <a class="btn btn-info" onclick="modalang(<?php echo $key->id ?>,'<?php echo $proposal ?>')" data-toggle="modal" href="#modalAnggota" title="Cek Anggota">
                                                            <i class="icon-group"></i>
                                                        </a>
                                                        <?php
                                                        // jika butuh persetujuan anggota
                                                        if ($key->status == 99) {
                                                            $notDeal = $this->model_proposal->getNotdeal($key->kd_usulan);
                                                            // jika semua anggota sudah setuju
                                                            if ($notDeal->num_rows() == 0) {
                                                        ?>
                                                                <a class="btn btn-success" href="<?php echo base_url(); ?>proposal/ajukan/<?php echo $key->id ?>/<?php echo $proposal ?>" title="Ajukan Usulan"><i class="icon-ok">Ajukan</i></a>
                                                    <?php
                                                            }
                                                        }
                                                    }
                                                    ?>

                                                    <?php
                                                    // jika di suruh revisi oleh fakultas atau lppmp
                                                    if ($key->status == 2 || $key->status == 12) {
                                                    ?>
                                                        <a class="btn btn-warning" href="<?php echo base_url(); ?>proposal/edit/<?php echo $key->id ?>/<?php echo $proposal ?>" title="ubah"><i class="icon-edit"></i></a>
                                                    <?php
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                        <?php $i++;
                                        } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="anggotaTab">
                            <table class="table table-hover table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Ketua</th>
                                        <th>Judul</th>
                                        <th>Tahun</th>
                                        <th>Setuju?</th>
                                        <th>Status proposal</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1;
                                    foreach ($anggota as $key) { ?>
                                        <tr>
                                            <td>
                                                <?php echo $i ?>
                                            </td>
                                            <td>
                                                <?php echo $key->nama ?>
                                            </td>
                                            <td>
                                                <a href="<?php echo $key->proposal ?>" target="_BLANK"><?php echo $key->judul ?></a>
                                            </td>
                                            <td>
                                                <?php echo $key->tahun ?>
                                            </td>
                                            <td>
                                                <?php if ($key->approval == 1) {
                                                    echo 'Ya';
                                                } elseif ($key->approval == 2) {
                                                    echo 'Tidak';
                                                } else {
                                                    echo '-';
                                                } ?>
                                            </td>
                                            <td>
                                                <?php echo getStatus($key->status) ?>
                                            </td>
                                            <td>
                                                <?php if ($key->approval == 0) { ?>
                                                    <a href="<?php echo base_url() ?>proposal/approve/<?php echo $key->id ?>/yes" class="btn btn-success"><i class="icon-ok"></i></a>
                                                    <a href="<?php echo base_url() ?>proposal/approve/<?php echo $key->id ?>/no" class="btn btn-danger"><i class="icon-remove"></i></a>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <?php $i++;
                                    } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" tabindex="-1" role="dialog" id="modalAnggota" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal" role="document">
        <div class="modal-content" id="modalView">

        </div>
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="modalLog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal" role="document">
        <div class="modal-content" id="modalViewlog">

        </div>
    </div>
</div>
<script>
    for (let index = 0; index < 3; index++) {
        $("#anggota" + index).autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: '<?php echo base_url('proposal/load_dosen_autocomplete'); ?>',
                    data: {
                        term: request.term
                    },
                    dataType: "json",
                    type: "POST",
                    success: function(data) {
                        response(data);
                    }
                });
            },
            select: function(evt, ui) {
                $('#anggota' + index).val(ui.item.value);
                $('#nama' + index).val(ui.item.name);
                $('#nid' + index).val(ui.item.save);
            }
        });
    }
</script>