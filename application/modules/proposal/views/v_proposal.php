<section id="inner-headline">
    <div class="container">
        <div class="row">
            <div class="span12">
                <div class="inner-heading">
                    <h2>Usulan <?php echo getUsulan($prop) ?></h2>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="content">
    <div class="container">
        <form class="form-horizontal" method="POST" action="<?php echo base_url(); ?>proposal/submit" onsubmit="document.getElementById('submitbutton').disabled=true;">
            <div class="row">
                <div class="span6">
                    <input type="hidden" id=prop name="usulan" value="<?php echo $prop ?>">
                    <div class="control-group">
                        <label class="span1" class="control-label" for="inputText">NID</label>
                        <div class="controls">
                            <input class="span4" type="text" name="nid" value="<?php echo $this->session->userdata('lppmp_session')['userid'] ?>" required readonly>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="span1" class="control-label" for="inputText">NIDN</label>
                        <div class="controls">
                            <input class="span4" type="text" name="nidn" value="<?php echo $this->session->userdata('lppmp_session')['nidn'] ?>" required readonly>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="span1" class="control-label" for="inputText">Nama</label>
                        <div class="controls">
                            <input class="span4" type="text" name="nama" value="<?php echo $this->session->userdata('lppmp_session')['name'] ?>" required readonly>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="span1" class="control-label" for="inputText">No Telp</label>
                        <div class="controls">
                            <input class="span4" type="text" name="tlp" value="<?php echo $this->session->userdata('lppmp_session')['hp'] ?>" required>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="span1" class="control-label" for="inputText">Email</label>
                        <div class="controls">
                            <input class="span4" type="email" name="email" value="<?php echo $this->session->userdata('lppmp_session')['email'] ?>" required>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="span1" class="control-label" for="inputText">Jabatan Fungsional</label>
                        <div class="controls">
                            <input class="span4" type="text" value="<?php echo getJabfung($this->session->userdata('lppmp_session')['jabfung']) ?>" required readonly>
                            <input type="hidden" id="jabfung" name="jabfung" value="<?php echo $this->session->userdata('lppmp_session')['jabfung'] ?>">
                        </div>
                    </div>
                </div>
                <div class="span6">
                    <div class="control-group">
                        <label class="span1" class="control-label" for="inputText">Jenis</label>
                        <div class="controls">
                            <select class="span4" name="jenis" onchange="return jnsChange(this)" required>
                                <option value="" selected hidden disabled>-- Pilih --</option>
                                <optgroup label="Internal">
                                    <option value="int">Universitas Bhayangkara Jakarta Raya</option>
                                </optgroup>
                                <optgroup label="External">
                                    <option value="mdr">Mandiri</option>
                                    <option value="hbh">Hibah</option>
                                    <option value="csr">CSR</option>
                                    <option value="spr">Sponsor</option>
                                </optgroup>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="span1" class="control-label" for="inputText">Judul</label>
                        <div class="controls">
                            <input class="span4" type="text" name="judul" value="" required>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="span1" class="control-label" for="inputText">URL Usulan</label>
                        <div class="controls">
                            <input class="span3" type="url" name="proposal" value="" required><a class="btn btn-info" href="https://bit.ly/format_proposal_laporan_penelitian2020" target="_BLANK" title="Format Proposal"><i class="icon-download-alt"></i></a>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="span1" class="control-label" for="inputText">URL Plagiarism</label>
                        <div class="controls">
                            <input class="span4" type="url" name="plagiarism" value="" required>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="span1" class="control-label" for="inputText">Anggota 1</label>
                        <div class="controls">
                            <input class="span3" type="text" onkeypress="return showplus(this,1)" id="anggota1" name="anggota1" value=""><button type="button" id="plus1" class="btn btn-blue" title="Tambah anggota"><i class="icon-ok"></i></button>
                            <input type="hidden" class="form-control" id="nid1" name="nid_anggota[0]" />
                            <input type="hidden" class="form-control" id="nama1" name="nama_anggota[0]" />
                        </div>
                    </div>
                    <div class="control-group" id="ang2">
                        <label class="span1" class="control-label" for="inputText">Anggota 2</label>
                        <div class="controls">
                            <input class="span3" type="text" onkeypress="return showplus(this,2)" id="anggota2" name="anggota2" value=""><button type="button" id="plus2" class="btn btn-blue" title="Tambah anggota"><i class="icon-ok"></i></button>
                            <input type="hidden" class="form-control" id="nid2" name="nid_anggota[1]" />
                            <input type="hidden" class="form-control" id="nama2" name="nama_anggota[1]" />
                        </div>
                    </div>
                    <div class="control-group" id="ang3">
                        <label class="span1" class="control-label" for="inputText">Anggota 3</label>
                        <div class="controls">
                            <input class="span3" type="text" onkeypress="return showplus(this,3)" id="anggota3" name="anggota3" value=""><button type="button" id="plus3" class="btn btn-blue" title="Tambah anggota"><i class="icon-ok"></i></button>
                            <input type="hidden" class="form-control" id="nid3" name="nid_anggota[2]" />
                            <input type="hidden" class="form-control" id="nama3" name="nama_anggota[2]" />
                        </div>
                    </div>
                    <div class="control-group" id="pendanaan">
                        <label class="span1" class="control-label" for="inputText">Dana</label>
                        <div class="controls">
                            <input class="span4" id="dana" type="text" required readonly>
                            <input type="hidden" id="dana_real" name="dana">
                        </div>
                    </div>
                </div>
                <div class="span12">
                    <div class="control-group">
                        <div class="controls" style="text-align: right;">
                            <button type="submit" class="btn btn-success" id="submitbutton">Ajukan</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
<script>
    $('#ang2').hide();
    $('#ang3').hide();
    $('#ang4').hide();
    $('#plus1').hide();
    $('#plus2').hide();
    $('#plus3').hide();
    $('#pendanaan').hide()

    function showplus(e, i) {
        if (e.value.length > 0) {
            $('#plus' + i).show();
        }
    }
    $('#plus1').click(function() {
        $('#anggota1')[0].readOnly = true
        $('#ang2').show();
        $('#plus1').hide();
    })
    $('#plus2').click(function() {
        $('#anggota2')[0].readOnly = true
        $('#ang3').show();
        $('#plus2').hide();
    })
    $('#plus3').click(function() {
        $('#anggota3')[0].readOnly = true
        $('#plus3').hide();
    })

    function jnsChange(e) {
        $('#pendanaan').show()
        if (e.value == 'int') {
            var jbf = $('#jabfung').val();
            var proposal = $('#prop').val();
            if (proposal == 'rsc') {

                if (jbf == 'TPD') {
                    dana = 5000000;
                } else if (jbf == 'SAH') {
                    dana = 10000000;
                } else if (jbf == 'LKT') {
                    dana = 15000000;
                } else if (jbf == 'LKK') {
                    dana = 20000000;
                } else if (jbf == 'BIG') {
                    dana = 25000000;
                } else {
                    dana = 0;
                }
                $('#dana').val('Rp ' + addCommas(dana))
                $('#dana_real').val(dana)

            } else if (proposal == 'dvt') {
                if ($('#anggota1').val() == '' && $('#anggota2').val() == '' && $('#anggota3').val() == '') {
                    $('#dana_real').val(2000000)
                    $('#dana').val('Rp ' + addCommas(2000000))
                }
                if ($('#anggota1').val() != '' && $('#anggota2').val() == '' && $('#anggota3').val() == '') {
                    $('#dana_real').val(4000000)
                    $('#dana').val('Rp ' + addCommas(4000000))
                }
                if ($('#anggota1').val() != '' && $('#anggota2').val() != '' && $('#anggota3').val() == '') {
                    $('#dana_real').val(6000000)
                    $('#dana').val('Rp ' + addCommas(6000000))
                }
                if ($('#anggota1').val() != '' && $('#anggota2').val() != '' && $('#anggota3').val() != '') {
                    $('#dana_real').val(8000000)
                    $('#dana').val('Rp ' + addCommas(8000000))
                }
                plus = 2000000
                $('#plus1').click(function() {
                    dana = parseInt($('#dana_real').val()) + parseInt(plus)
                    $('#dana_real').val(dana)
                    $('#dana').val('Rp ' + addCommas(dana))
                })
                $('#plus2').click(function() {
                    dana = parseInt($('#dana_real').val()) + parseInt(plus)
                    $('#dana_real').val(dana)
                    $('#dana').val('Rp ' + addCommas(dana))
                })
                $('#plus3').click(function() {
                    dana = parseInt($('#dana_real').val()) + parseInt(plus)
                    $('#dana_real').val(dana)
                    $('#dana').val('Rp ' + addCommas(dana))
                })

            }
        } else {
            $('#dana_real').val('')
            $('#dana').val('')
            $('#pendanaan').hide()
        }
    }


    function addCommas(nStr) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }
    $(document).ready(function() {
        $(function() {
            $("#anggota1").autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: '<?php echo base_url('proposal/load_dosen_autocomplete'); ?>',
                        data: {
                            term: request.term
                        },
                        dataType: "json",
                        type: "POST",
                        success: function(data) {
                            response(data);
                        }
                    });
                },
                select: function(evt, ui) {
                    console.log(ui);
                    $('#anggota1').val(ui.item.value);
                    $('#nama1').val(ui.item.name);
                    $('#nid1').val(ui.item.save);
                }
            });
            $("#anggota2").autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: '<?php echo base_url('proposal/load_dosen_autocomplete'); ?>',
                        data: {
                            term: request.term
                        },
                        dataType: "json",
                        type: "POST",
                        success: function(data) {
                            response(data);
                        }
                    });
                },
                select: function(evt, ui) {
                    console.log(ui);
                    $('#anggota2').val(ui.item.value);
                    $('#nama2').val(ui.item.name);
                    $('#nid2').val(ui.item.save);
                }
            });
            $("#anggota3").autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: '<?php echo base_url('proposal/load_dosen_autocomplete'); ?>',
                        data: {
                            term: request.term
                        },
                        dataType: "json",
                        type: "POST",
                        success: function(data) {
                            response(data);
                        }
                    });
                },
                select: function(evt, ui) {
                    console.log(ui);
                    $('#anggota3').val(ui.item.value);
                    $('#nama3').val(ui.item.name);
                    $('#nid3').val(ui.item.save);
                }
            });
        });
    });
</script>