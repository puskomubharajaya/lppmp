<section id="inner-headline">
    <div class="container">
        <div class="row">
            <div class="span12">
                <div class="inner-heading">
                    <h2>Ubah Usulan <?php echo getUsulan($prop) ?></h2>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="content">
    <div class="container">
        <form class="form-horizontal" method="POST" action="<?php echo base_url(); ?>proposal/update" onsubmit="document.getElementById('submitbutton').disabled=true;">
            <div class="row">
                <div class="span6">
                    <input type="hidden" id=prop name="usulan" value="<?php echo $prop ?>">
                    <input type="hidden" name="id" value="<?php echo $proposal->id ?>">
                    <input type="hidden" name="kd_usulan" value="<?php echo $proposal->kd_usulan ?>">
                    <input type="hidden" name="status" value="<?php echo $proposal->status ?>">
                    <div class="control-group">
                        <label class="span1" class="control-label" for="inputText">NID</label>
                        <div class="controls">
                            <input class="span4" type="text" name="nid" value="<?php echo $proposal->nid ?>" required readonly>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="span1" class="control-label" for="inputText">NIDN</label>
                        <div class="controls">
                            <input class="span4" type="text" name="nidn" value="<?php echo $proposal->nidn ?>" required readonly>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="span1" class="control-label" for="inputText">Nama</label>
                        <div class="controls">
                            <input class="span4" type="text" name="nama" value="<?php echo $proposal->nama ?>" required readonly>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="span1" class="control-label" for="inputText">No Telp</label>
                        <div class="controls">
                            <input class="span4" type="text" name="tlp" value="<?php echo $proposal->tlp ?>" required>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="span1" class="control-label" for="inputText">Email</label>
                        <div class="controls">
                            <input class="span4" type="email" name="email" value="<?php echo $proposal->email ?>" required>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="span1" class="control-label" for="inputText">Jabatan Fungsional</label>
                        <div class="controls">
                            <input class="span4" type="text" value="<?php echo getJabfung($proposal->jabfung) ?>" required readonly>
                            <input type="hidden" id="jabfung" name="jabfung" value="<?php echo $proposal->jabfung ?>">
                        </div>
                    </div>
                </div>
                <div class="span6">
                    <div class="control-group">
                        <label class="span1" class="control-label" for="inputText">Judul</label>
                        <div class="controls">
                            <input class="span4" type="text" name="judul" value="<?php echo $proposal->judul ?>" required>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="span1" class="control-label" for="inputText">URL Usulan</label>
                        <div class="controls">
                            <input class="span3" type="url" name="proposal" value="<?php echo $proposal->proposal ?>" required><a class="btn btn-info" href="https://bit.ly/format_proposal_laporan_penelitian2020" target="_BLANK" title="Format Proposal"><i class="icon-download-alt"></i></a>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="span1" class="control-label" for="inputText">URL Plagiarism</label>
                        <div class="controls">
                            <input class="span4" type="url" name="plagiarism" value="<?php echo $proposal->plagiarism ?>" required>
                        </div>
                    </div>

                </div>
                <div class="span12">
                    <div class="control-group">
                        <div class="controls" style="text-align: right;">
                            <button type="submit" class="btn btn-success" id="submitbutton">Ajukan</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>