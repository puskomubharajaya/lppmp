<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="gridSystemModalLabel">Daftar Anggota</h4>
</div>
<form class="form-horizontal" action="<?php echo base_url(); ?>proposal/ubahanggota" method="post" onsubmit="document.getElementById('submitbutton').disabled=true;">
    <div class="modal-body">
        <div class="row">
            <div class="span6">
                <?php $i = 0;
                foreach ($listAnggota->result() as $key) { ?>
                    <div class="control-group">
                        <label class="span1" class="control-label" for="inputText">Anggota <?php echo $i + 1 ?></label>
                        <div class="controls">
                            <?php if ($key->approval == 1) {
                                $icon = '<i style="color:green" class="icon-ok"> diterima</i>';
                                $disabled = 'disabled';
                            } elseif ($key->approval == 2) {
                                $icon = '<i style="color:red" class="icon-remove"> ditolak</i>';
                                $disabled = '';
                            } else {
                                $icon = '<i class="icon-spinner"> menunggu</i>';
                                $disabled = 'disabled';
                            } ?>
                            <input class="span3" type="text" id="anggota<?php echo $i ?>" value="<?php echo $key->nid ?> - <?php echo $key->nama ?>" <?php echo $disabled ?> required><?php echo $icon ?>
                            <input type="hidden" id="nid<?php echo $i ?>" name="nid_anggota[<?php echo $i ?>]">
                            <input type="hidden" id="nama<?php echo $i ?>" name="nama_anggota[<?php echo $i ?>]">
                            <input type="hidden" name="id_anggota[<?php echo $i ?>]" value="<?php echo $key->id ?>" <?php echo $disabled ?>>
                            <input type="hidden" name="kd_usulan" value="<?php echo $key->kd_usulan ?>">
                        </div>
                    </div>
                <?php $i++;
                } ?>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <?php if ($notDeal->num_rows() > 0) { ?>
            <button type="submit" class="btn btn-primary" id="submitbutton">Ubah</button>
        <?php } ?>
    </div>
</form>