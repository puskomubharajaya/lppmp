<?php defined('BASEPATH') or exit('No direct script access allowed');

class Error_404 extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        $this->output->set_status_header('404');
        $data['page'] = 'v_404_error';
        $this->load->view('template/homepage', $data);
    }
    public function alertLogin()
    {
        $data['page'] = 'v_alert_login';
        $this->load->view('template/homepage', $data);
    }
}
