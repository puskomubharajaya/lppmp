<section id="inner-headline">
    <div class="container">
        <div class="row">
            <div class="span12">
                <div class="inner-heading">
                    <h2>Maaf!</h2>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="content">
    <div class="container">
        <div class="row">
            <div class="span12">
                <h2 class="aligncenter">Anda belum sign in</h3>
                    <p class="aligncenter">
                        Sign in terlebih dahulu untuk melanjutkan
                    </p>
                    <center><a class="btn btn-red" href="#mySignin" data-toggle="modal">Sign in</a></center>
            </div>
        </div>
    </div>
</section>
<div id="mySignin" class="modal styled hide fade" tabindex="-1" role="dialog" aria-labelledby="mySigninModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 id="mySigninModalLabel"><strong>Login</strong></h4>
    </div>
    <div class="modal-body">
        <form class="form-horizontal" method="POST" action="<?php echo base_url(); ?>auth/login" onsubmit="document.getElementById('submitbutton').disabled=true;">
            <div class="control-group">
                <label class="control-label" for="inputText">Username</label>
                <div class="controls">
                    <input type="text" id="inputText" placeholder="Username" name="username" required>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputSigninPassword">Password</label>
                <div class="controls">
                    <input type="password" id="inputSigninPassword" placeholder="Password" name="password" required>
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <button type="submit" class="btn" id="submitbutton">Sign in</button>
                </div>
            </div>
        </form>
    </div>
</div>