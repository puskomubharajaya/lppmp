
<section id="inner-headline">
    <div class="container">
        <div class="row">
            <div class="span12">
                <div class="inner-heading">
                    <h2>404</h2>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="content">
    <div class="container">
        <div class="row">
            <div class="span12">
                <h2 class="aligncenter">404 Halaman yang anda minta tidak tersedia.</h3>
                    <p class="aligncenter">
                        Mohon cek kembali url anda, atau anda dapat langsung kembali ke Halaman Home, Terimakasih!
                    </p>
            </div>
        </div>
    </div>
</section>