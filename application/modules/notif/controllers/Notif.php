<?php defined('BASEPATH') or exit('No direct script access allowed');

class Notif extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('lppmp_session') || is_null($this->session->userdata('lppmp_session'))) {
            redirect('errors/error_404/alertlogin');
        }
    }
    public function index()
    {
        $data['notif']  = $this->model_crud->getdetail('tbl_notification','to', $this->session->userdata('lppmp_session')['userid'],'id','DESC')->result();
        $data['page']  = 'v_notif';
        $this->load->view('template/homepage', $data);
    }
    public function allNotif()
    {
        $data['notif']  = $this->model_crud->getdetail('tbl_notification','to', $this->session->userdata('lppmp_session')['userid'],'id','DESC')->result();
        $data['page']  = 'v_notif_dashboard';
        $this->load->view('template/dashboard', $data);
    }
    public function read_notif($kd, $id)
    {
        $checkKd1 = $this->model_crud->getdetail('tbl_penelitian', 'kd_usulan', $kd, 'id', 'DESC');
        $checkKd2 = $this->model_crud->getdetail('tbl_pengabdian', 'kd_usulan', $kd, 'id', 'DESC');
        if ($checkKd1->num_rows() > 0) {
            $this->_is_read($id);
            redirect('proposal/myProposal/rsc');
        } elseif ($checkKd2->num_rows() > 0) {
            $this->_is_read($id);
            redirect('proposal/myProposal/dvt');
        } else {
            echo '<script>alert("Data tidak di temukan!");history.go(-1);</script>';
        }
    }
    public function buka_notif($kd, $id)
    {
        $checkKd1 = $this->model_crud->getdetail('tbl_penelitian', 'kd_usulan', $kd, 'id', 'DESC');
        $checkKd2 = $this->model_crud->getdetail('tbl_pengabdian', 'kd_usulan', $kd, 'id', 'DESC');
        if ($checkKd1->num_rows() > 0) {
            $this->_is_read($id);
            redirect('forms/proposal/rsc');
        } elseif ($checkKd2->num_rows() > 0) {
            $this->_is_read($id);
            redirect('forms/proposal/dvt');
        } else {
            echo '<script>alert("Data tidak di temukan!");history.go(-1);</script>';
        }
    }
    public function _is_read($id)
    {
        $data = array(
            'is_read' => 1,
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => $this->session->userdata('lppmp_session')['userid']
        );

        $this->model_crud->updatedata('tbl_notification', 'id', $id, $data);
    }
}
