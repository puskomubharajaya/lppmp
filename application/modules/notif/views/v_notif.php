<section id="inner-headline">
    <div class="container">
        <div class="row">
            <div class="span12">
                <div class="inner-heading">
                    <h2>Pemberitahuan</h2>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="content">
    <div class="container">
        <table class="table  table-hover table-striped">
            <tbody>
                <?php foreach ($notif as $key) { ?>
                    <tr>
                        <td><?php echo $key->content ?></td>
                        <td><?php echo time_ago($key->created_at) ?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</section>