<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>Pemberitahuan <small><?php echo $this->session->userdata('lppmp_session')['name'] ?></small></h3>
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Daftar<small>Pemberitahuan</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table id="datatable-responsive" class="table table-striped dt-responsive" cellspacing="0" width="100%">
                        <tbody>
                            <?php
                            foreach ($notif as $key) {
                            ?>
                                <tr>
                                    <td><?php echo $key->content ?></td>
                                    <td><?php echo time_ago($key->created_at) ?></td>
                                </tr>
                            <?php
                            } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>