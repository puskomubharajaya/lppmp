<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>LEMBAGA PENELITIAN PENGABDIAN KEPADA MASYARAKAT DAN PUBLIKASI</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="" />
    <meta name="author" content="" />

    <!-- css -->
    <link href="https://fonts.googleapis.com/css?family=Noto+Serif:400,400italic,700|Open+Sans:300,400,600,700" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/homepage/css/bootstrap.css" rel="stylesheet" />
    <link href="<?php echo base_url() ?>assets/homepage/css/bootstrap-responsive.css" rel="stylesheet" />
    <link href="<?php echo base_url() ?>assets/homepage/css/fancybox/jquery.fancybox.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/homepage/css/jcarousel.css" rel="stylesheet" />
    <link href="<?php echo base_url() ?>assets/homepage/css/flexslider.css" rel="stylesheet" />
    <link href="<?php echo base_url() ?>assets/homepage/css/cslider.css" rel="stylesheet" />
    <link href="<?php echo base_url() ?>assets/homepage/css/style.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/v/dt/dt-1.10.21/r-2.2.5/datatables.min.css" rel="stylesheet" />
    <!-- Theme skin -->
    <link id="t-colors" href="<?php echo base_url() ?>assets/homepage/skins/default.css" rel="stylesheet" />
    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url('uploads/logo/apple-touch-icon-144-precomposed.png') ?>" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url('uploads/logo/apple-touch-icon-114-precomposed.png') ?>" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url('uploads/logo/apple-touch-icon-72-precomposed.png') ?>" />
    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url('uploads/logo/apple-touch-icon-57-precomposed.png') ?>" />
    <link rel="shortcut icon" href="<?php echo base_url('uploads/logo/logo_ubj.png') ?>" />

    <script src="<?php echo base_url() ?>assets/homepage/js/jquery.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
</head>

<body>
    <div id="wrapper">
        <!-- toggle top area -->
        <div class="hidden-top">
            <div class="hidden-top-inner container">
                <div class="row">
                    <div class="span12">
                        <ul>
                            <li><strong>Informasi lebih lanjut, hubungi kami</strong></li>
                            <li>Kantor: Jl. Raya Perjuangan Bekasi Utara, Kota Bekasi</li>
                            <li>Telp <i class="icon-phone"></i> (123) 456-7890 - (123) 555-7891</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- end toggle top area -->
        <style>
            span#name_user {
                font-size: 1em;
            }

            span.icon-bell {
                position: relative;
                font-size: 1em;
                cursor: pointer;
            }

            span.icon-comment {
                position: absolute;
                font-size: 1.5em;
                top: -15px;
                color: red;
                right: -22px;
            }

            span.num {
                position: absolute;
                font-size: 0.8em;
                top: -10px;
                color: #fff;
                right: -8px;
            }

            #ui-id-1 {
                z-index: 99999999 !important;
                background-color: #fff !important;
                width: 500px !important;
            }

            ul#ui-id-1 {
                list-style: none !important;
                padding-left: 20px !important;
                padding-right: 20px !important;
            }

            #ui-id-2 {
                z-index: 99999999 !important;
                background-color: #fff !important;
                width: 500px !important;
            }

            ul#ui-id-2 {
                list-style: none !important;
                padding-left: 20px !important;
                padding-right: 20px !important;
            }


            #ui-id-3 {
                z-index: 99999999 !important;
                background-color: #fff !important;
                width: 500px !important;
            }

            ul#ui-id-3 {
                list-style: none !important;
                padding-left: 20px !important;
                padding-right: 20px !important;
            }
        </style>
        <?php
        $arr = array(
            'to'      => $this->session->userdata('lppmp_session')['userid'],
            'is_read' => 0,
        );
        $notif = $this->model_crud->getMoreWhere('tbl_notification', $arr, 'id', 'DESC'); ?>
        <!-- start header -->
        <header>
            <div class="container">
                <!-- hidden top area toggle link -->
                <div id="header-hidden-link">
                    <a href="#" class="toggle-link" title="Info" data-target=".hidden-top"><i></i>Open</a>
                </div>
                <!-- end toggle link -->
                <div class="row nomargin">
                    <div class="span12">
                        <div class="headnav">
                            <ul style="display: inline-block;">
                                <?php if (!$this->session->userdata('lppmp_session') || is_null($this->session->userdata('lppmp_session'))) { ?>
                                    <li><a id="dLabel" href="#mySignin" data-toggle="modal" title="Masuk">Sign in</a></li>
                                <?php } else { ?>
                                    <li class="dropdown">
                                        <span style="text-decoration: none;" class="icon-bell" data-toggle="dropdown" role="button">
                                            <span class="icon-comment"></span>
                                            <span class="num"><?php echo $notif->num_rows() ?></span>
                                        </span>
                                        <ul class="dropdown-menu" style="min-width: 220px;z-index: 2000;">
                                            <?php if ($notif->num_rows() == 0) { ?>
                                                <li style="margin-left: 0px;text-align: center;">
                                                    <a style="padding: 5px;" href="#">Tidak ada pemberitahuan baru</a>
                                                </li>
                                            <?php } else { ?>
                                                <?php foreach ($notif->result() as $key) { ?>
                                                    <li style="margin-left: 0px;">
                                                        <a href="<?php echo base_url() ?>notif/read_notif/<?php echo $key->kd_usulan ?>/<?php echo $key->id ?>" style="white-space: inherit;"><?php echo $key->content ?><br>
                                                            <div style="text-align: right;"><small><?php echo time_ago($key->created_at) ?></small></div>
                                                        </a>
                                                    </li>
                                                <?php } ?>
                                            <?php } ?>
                                            <li style="margin-left: 0px;text-align: center;">
                                                <a style="padding: 5px;" href="<?php echo base_url(); ?>notif">Lihat semua</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="dropdown">
                                        <span id="name_user">Hai,&nbsp;</span>
                                        <a href="#" style="text-decoration: none;font-size: 1em;" title="Akun" data-toggle="dropdown" role="button">
                                            <?php echo $this->session->userdata('lppmp_session')['name'] ?> <i class="icon-angle-down"></i>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li style="margin-left: 0px;"><a href="<?php echo base_url(); ?>auth/logout" data-toggle="modal" title="keluar">Sign out</a></li>
                                        </ul>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <!-- Sign in Modal -->
                        <div id="mySignin" class="modal styled hide fade" tabindex="-1" role="dialog" aria-labelledby="mySigninModalLabel" aria-hidden="true">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 id="mySigninModalLabel"><strong>Login</strong></h4>
                            </div>
                            <div class="modal-body">
                                <form class="form-horizontal" method="POST" action="<?php echo base_url(); ?>auth/login">
                                    <div class="control-group">
                                        <label class="control-label" for="inputText">Username</label>
                                        <div class="controls">
                                            <input type="text" id="inputText" placeholder="Username" name="username" required>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="inputSigninPassword">Password</label>
                                        <div class="controls">
                                            <input type="password" id="inputSigninPassword" placeholder="Password" name="password" required>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <div class="controls">
                                            <button type="submit" class="btn" id="submitlogin" onsubmit="document.getElementById('submitlogin').disabled=true;">Sign in</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- end signin modal -->
                    </div>
                </div>
                <div class="row">
                    <div class="span4">
                        <div class="logo">
                            <a href="<?php echo base_url() ?>"><img style="height:70px" src="<?php echo base_url('uploads/logo/new_ubj.png') ?>" alt="LPPMP Universitas Bhayangkara Jakarta Raya" class="logo" /></a>
                        </div>
                    </div>
                    <div class="span8">
                        <div class="navbar navbar-static-top">
                            <div class="navigation">
                                <nav>
                                    <ul class="nav topnav">
                                        <li class="<?php echo $this->uri->segment(1) == null ? 'active' : '' ?>">
                                            <a href="<?php echo base_url() ?>">Home </a>
                                        </li>
                                        <li class="<?php echo $this->uri->segment(1) == 'profile' ? 'active' : '' ?>">
                                            <a href="<?php echo base_url('profile') ?>">Tentang Kami </a>
                                        </li>
                                        <!-- <li class="dropdown">
                                            <a href="#">Pengabdian Masyarakat <i class="icon-angle-down"></i></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="about.html">Pengajuan Proposal Penelitian</a></li>
                                                <li><a href="pricingbox.html">Laporan hasil penelitian</a></li>
                                                <li><a href="pricingbox.html">Output Penelitian</a></li>
                                            </ul>
                                        </li>
                                        <li class="dropdown">
                                            <a href="#">Penelitian <i class="icon-angle-down"></i></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="about.html">Pengajuan Proposal Penelitian</a></li>
                                                <li><a href="pricingbox.html">Laporan hasil penelitian</a></li>
                                                <li><a href="pricingbox.html">Output Penelitian</a></li>
                                            </ul>
                                        </li>
                                        <li class="dropdown">
                                            <a href="#">Publikasi <i class="icon-angle-down"></i></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="about.html">Repositori centra HKI perfakultas/tahun</a></li>
                                                <li><a href="pricingbox.html">Pengajuan pendanaan publikasi</a></li>
                                                <li><a href="pricingbox.html">Pengajuan insentif publikasi</a></li>
                                            </ul>
                                        </li> -->
                                        <li class="dropdown <?php echo $this->uri->segment(2) == 'usulan' ? 'active' : '' ?>">
                                            <a href="#">Usulan Baru <i class="icon-angle-down"></i></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="<?php echo base_url('proposal/usulan/dvt') ?>">Pengabdian</a></li>
                                                <li><a href="<?php echo base_url('proposal/usulan/rsc') ?>">penelitian</a></li>
                                            </ul>
                                        </li>
                                        <li class="dropdown <?php echo $this->uri->segment(2) == 'myproposal' ? 'active' : '' ?>">
                                            <a href="#">Usulan <i class="icon-angle-down"></i></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="<?php echo base_url('proposal/myproposal/dvt') ?>">Pengabdian</a></li>
                                                <li><a href="<?php echo base_url('proposal/myproposal/rsc') ?>">penelitian</a></li>
                                            </ul>
                                        </li>
                                        <li class="<?php echo $this->uri->segment(1) == 'blog' ? 'active' : '' ?>">
                                            <a href="<?php echo base_url('blog') ?>">Blog </a>
                                        </li>
                                        <li class="<?php echo $this->uri->segment(1) == 'kontak' ? 'active' : '' ?>">
                                            <a href="<?php echo base_url('kontak') ?>">Kontak </a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                            <!-- end navigation -->
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- end header -->
        <?php $this->load->view($page) ?>
        <footer>
            <div class="container">
                <div class="row">
                    <div class="span4">
                        <div class="widget">
                            <h5 class="widgetheading">Tentang Kami</h5>
                            <p class="link-list">
                                LPPMP UBJ merupakan unit yang memberdayakan sumber daya dalam melakukan penelitian inovatif yang mempertimbangkan kearifan lokal serta pengabdian masyarakat yang tepat sasaran.
                            </p>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="widget">
                            <h5 class="widgetheading">Important stuff</h5>
                            <ul class="link-list">
                                <li><a href="#">Press release</a></li>
                                <li><a href="#">Terms and conditions</a></li>
                                <li><a href="#">Privacy policy</a></li>
                                <li><a href="#">Career center</a></li>
                                <li><a href="#">Flattern forum</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="widget">
                            <h5 class="widgetheading">Kantak</h5>
                            <address>
                                <strong>LPPMP Ubhara Jaya</strong><br>
                                Jl. Raya Perjuangan Bekasi Utara, Kota Bekasi<br>
                                Jawa Barat 17121 Indonesia
                            </address>
                            <p>
                                <i class="icon-phone"></i> (123) 456-7890 - (123) 555-7891 <br>
                                <i class="icon-envelope-alt"></i> lppmp@ubharajaya.ac.id
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div id="sub-footer">
                <div class="container">
                    <div class="row">
                        <div class="span6">
                            <div class="copyright">
                                <p>
                                    <span>&copy; Flattern - All right reserved.</span>
                                </p>
                                <div class="credits">
                                    Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
                                </div>
                            </div>
                        </div>
                        <div class="span6">
                            <ul class="social-network">
                                <li><a href="#" data-placement="bottom" title="Facebook"><i class="icon-facebook icon-square"></i></a></li>
                                <li><a href="#" data-placement="bottom" title="Twitter"><i class="icon-twitter icon-square"></i></a></li>
                                <li><a href="#" data-placement="bottom" title="Linkedin"><i class="icon-linkedin icon-square"></i></a></li>
                                <li><a href="#" data-placement="bottom" title="Pinterest"><i class="icon-pinterest icon-square"></i></a></li>
                                <li><a href="#" data-placement="bottom" title="Google plus"><i class="icon-google-plus icon-square"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <a href="#" class="scrollup"><i class="icon-chevron-up icon-square icon-32 active"></i></a>
    <!-- javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url() ?>assets/homepage/js/jquery.easing.1.3.js"></script>
    <script src="<?php echo base_url() ?>assets/homepage/js/bootstrap.js"></script>
    <script src="<?php echo base_url() ?>assets/homepage/js/jcarousel/jquery.jcarousel.min.js"></script>
    <script src="<?php echo base_url() ?>assets/homepage/js/jquery.fancybox.pack.js"></script>
    <script src="<?php echo base_url() ?>assets/homepage/js/jquery.fancybox-media.js"></script>
    <script src="<?php echo base_url() ?>assets/homepage/js/google-code-prettify/prettify.js"></script>
    <script src="<?php echo base_url() ?>assets/homepage/js/portfolio/jquery.quicksand.js"></script>
    <script src="<?php echo base_url() ?>assets/homepage/js/portfolio/setting.js"></script>
    <script src="<?php echo base_url() ?>assets/homepage/js/jquery.flexslider.js"></script>
    <script src="<?php echo base_url() ?>assets/homepage/js/jquery.nivo.slider.js"></script>
    <script src="<?php echo base_url() ?>assets/homepage/js/modernizr.custom.js"></script>
    <script src="<?php echo base_url() ?>assets/homepage/js/jquery.ba-cond.min.js"></script>
    <script src="<?php echo base_url() ?>assets/homepage/js/jquery.slitslider.js"></script>
    <script src="<?php echo base_url() ?>assets/homepage/js/jquery.cslider.js"></script>
    <script src="<?php echo base_url() ?>assets/homepage/js/animate.js"></script>
    <script src="https://cdn.datatables.net/v/dt/dt-1.10.21/r-2.2.5/datatables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#myTable').DataTable();
        });
    </script>
    <!-- Template Custom JavaScript File -->
    <script src="<?php echo base_url() ?>assets/homepage/js/custom.js"></script>

</body>

</html>