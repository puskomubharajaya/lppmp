<?php defined('BASEPATH') or exit('No direct script access allowed');

use Endroid\QrCode\ErrorCorrectionLevel;
use Endroid\QrCode\LabelAlignment;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\Response\QrCodeResponse;

class Forms extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_forms');
        if (!$this->session->userdata('lppmp_session')) {
            redirect('/', 'refresh');
        }
    }
    public function proposal($usulan)
    {
        $is_Reviewer = $this->model_crud->getdetail('tbl_reviewer', 'nid', $this->session->userdata('lppmp_session')['userid'], 'id', 'desc');
        if ($this->session->userdata('lppmp_session')['group'] == 9) {
            $data['usulan'] = $this->model_forms->getDatabyFak(getTblusulan($usulan), 'id', 'DESC');
            $data['user'] = 'fakultas';
        } else if ($this->session->userdata('lppmp_session')['group'] == 23) {
            $data['usulan'] = $this->model_forms->getDataforRektor(getTblusulan($usulan), 'id', 'DESC');
            $data['user'] = 'rektor';
        } else {
            if ($is_Reviewer->num_rows() > 0) {
                $data['usulan'] = $this->model_forms->getDatabyrev(getTblusulan($usulan), $this->session->userdata('lppmp_session')['userid'], 'id', 'DESC');
                $data['user'] = 'reviewer';
            } else {
                $data['usulan'] = $this->model_crud->getData(getTblusulan($usulan), 'id', 'DESC');
                $data['user'] = 'lppmp';
            }
        }
        $data['proposal'] = $usulan;
        $data['page'] = 'v_forms';
        $this->load->view('template/dashboard', $data);
    }

    public function approve($usulan, $id)
    {
        $proposal         = $this->model_crud->getDetail(getTblusulan($usulan), 'id', $id, 'id', 'ASC')->row();
        $data['anggota']  = $this->model_crud->getDetail('tbl_anggota', 'kd_usulan', $proposal->kd_usulan, 'id', 'ASC');
        $data['proposal'] = $proposal;
        $data['usulan']   = $usulan;
        $this->load->view('modal_approve', $data);
    }

    public function plot_reviewer($usulan, $id)
    {
        $proposal         = $this->model_crud->getDetail(getTblusulan($usulan), 'id', $id, 'id', 'ASC')->row();
        $data['reviewer'] = $this->model_crud->getDetail('tbl_reviewer', 'fakultas', $proposal->fakultas, 'id', 'ASC')->result();
        $data['anggota']  = $this->model_crud->getDetail('tbl_anggota', 'kd_usulan', $proposal->kd_usulan, 'id', 'ASC');
        $data['proposal'] = $proposal;
        $data['usulan']   = $usulan;
        $this->load->view('modal_ploting', $data);
    }

    public function approve_lppmp($usulan, $id)
    {
        $proposal         = $this->model_crud->getDetail(getTblusulan($usulan), 'id', $id, 'id', 'ASC')->row();
        $data['reviewer'] = $this->model_crud->getDetail('tbl_reviewer_hasil', 'kd_usulan', $proposal->kd_usulan, 'id', 'ASC')->result();
        $data['anggota']  = $this->model_crud->getDetail('tbl_anggota', 'kd_usulan', $proposal->kd_usulan, 'id', 'ASC');
        $data['proposal'] = $proposal;
        $data['usulan']   = $usulan;
        $this->load->view('modal_approve_lppmp', $data);
    }

    public function lihat_reviewer($usulan, $id)
    {
        $proposal         = $this->model_crud->getDetail(getTblusulan($usulan), 'id', $id, 'id', 'ASC')->row();
        $data['allreviewer'] = $this->model_crud->getDetail('tbl_reviewer', 'fakultas', $proposal->fakultas, 'id', 'ASC')->result();
        $data['reviewer']    = $this->model_crud->getDetail('tbl_reviewer_hasil', 'kd_usulan', $proposal->kd_usulan, 'id', 'ASC');
        $data['notDeal']     = $this->model_forms->getNotdeal_reviewer($proposal->kd_usulan);
        $this->load->view('modal_reviewer', $data);
    }

    public function nilai_reviewer($usulan, $id)
    {
        $proposal         = $this->model_crud->getDetail(getTblusulan($usulan), 'id', $id, 'id', 'ASC')->row();
        $arr = array(
            'kd_usulan'  => $proposal->kd_usulan,
            'nid'        => $this->session->userdata('lppmp_session')['userid'],
            'is_deleted' => 0
        );
        $data['anggota']  = $this->model_crud->getDetail('tbl_anggota', 'kd_usulan', $proposal->kd_usulan, 'id', 'ASC');
        $data['reviewer']  = $this->model_crud->getMoreWhere('tbl_reviewer_hasil', $arr, 'id', 'DESC')->row();
        $data['proposal'] = $proposal;
        $data['usulan']   = $usulan;
        $this->load->view('modal_nilai', $data);
    }

    public function persetujuan()
    {
        if ($this->input->post('status') == 1) {
            $current = 'Telah disetujui oleh fakultas';
            $next    = 'Menunggu hasil review LPPMP';
            $ntf    = ' setuju dengan usulan anda';
        } else
        if ($this->input->post('status') == 2) {
            $current = 'Usulan dikembalikan oleh fakultas';
            $next    = 'Revisi usulan';
            $ntf    = ' menyarankan anda untuk merevisi usulan anda';
        } else
        if ($this->input->post('status') == 11) {
            $current = 'Telah disetujui oleh LPPMP';
            $next    = 'Menunggu keputusan Rektor';
            $ntf    = ' setuju dengan usulan anda';
        } else
        if ($this->input->post('status') == 12) {
            $current = 'Usulan dikembalikan oleh LPPMP';
            $next    = 'Revisi usulan';
            $ntf    = ' menyarankan anda untuk merevisi usulan anda';
        } else
        if ($this->input->post('status') == 13) {
            $current = 'Usulan ditolak oleh LPPMP';
            $next    = '';
            $ntf    = ' menolak usulan anda';
        }

        $data2 = array(
            "status"      => $this->input->post('status'),
            "updated_at"  => date('Y-m-d H:i:s'),
            "updated_by"  => $this->session->userdata('lppmp_session')['userid']
        );

        // membuat notifikasi ke fakultas
        $this->_createNotif($this->input->post('kd_usulan'), $this->input->post('nid'), $ntf, 1);
        // menyimpan riwayat status pengajuan
        $this->_createLogstatus($this->input->post('usulan'), $this->input->post('kd_usulan'), $this->input->post('status'), $current, $next, $this->input->post('note'));
        $this->model_crud->updateData(getTblusulan($this->input->post('usulan')), 'id', $this->input->post('id_usulan'), $data2);
        echo "<script>alert('Berhasil!');history.go(-1);</script>";
    }

    public function ploting()
    {
        $ntf    = ' menyarankan anda untuk mereview usulan';
        for ($i = 0; $i < count($this->input->post('reviewer')); $i++) {
            $explode = explode('-', $this->input->post('reviewer')[$i]);
            $data = array(
                "jenis_usulan" => $this->input->post('usulan'),
                "kd_usulan"    => $this->input->post('kd_usulan'),
                "reviewer"     => 'Reviewer ' . ($i + 1),
                "nid"          => $explode[0],
                "nama"         => $explode[1],
                "approval"     => 0,
                "created_at"   => date('Y-m-d H:i:s'),
                "created_by"   => $this->session->userdata('lppmp_session')['userid'],
                "is_deleted"   => 0
            );
            // membuat notifikasi ke reviewer terpilih
            $this->_createNotif($this->input->post('kd_usulan'), $explode[0], $ntf, 2);
            $this->model_crud->insertData('tbl_reviewer_hasil', $data);
        }
        echo "<script>alert('Berhasil!');history.go(-1);</script>";
    }

    public function approve_reviewer($id, $option)
    {
        $reviewer = $this->model_crud->getdetail('tbl_reviewer_hasil', 'id', $id, 'id', 'DESC')->row();
        if ($option == 'yes') {
            $approval = 1;
            // notifikasi ke lppmp usulan jika setuju
            $this->_createNotif($reviewer->kd_usulan, $reviewer->created_by, ' setuju sebagai reviewer', 1);
        } elseif ($option == 'no') {
            $approval = 2;
            // notifikasi ke lppmp jika tidak setuju
            $this->_createNotif($reviewer->kd_usulan, $reviewer->created_by, ' menolak sebagai reviewer', 1);
        }
        $data = array(
            'approval'   => $approval,
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => $this->session->userdata('lppmp_session')['userid']
        );

        // menerima/menolak ajakan sebagai reviewer
        $this->model_crud->updatedata('tbl_reviewer_hasil', 'id', $id, $data);
        echo "<script>alert('Berhasil!');history.go(-1);</script>";
    }

    public function ubahreviewer()
    {
        foreach ($this->input->post('reviewer') as $key => $value) {
            $explode = explode('-', $value);
            if ($value != '') {
                $rvw = array(
                    'nid'        => $explode[0],
                    'nama'       => $explode[1],
                    'approval'   => 0,
                    "updated_at" => date('Y-m-d H:i:s'),
                    "updated_by" => $this->session->userdata('lppmp_session')['userid']
                );
                // membuat notifikasi reviewer yang di undang
                $this->_createNotif($this->input->post('kd_usulan'), $explode[0], '  menyarankan anda untuk mereview usulan', 1);
                // mengganti reviewer
                $this->model_crud->updatedata('tbl_reviewer_hasil', 'id', $this->input->post('id_reviewer')[$key], $rvw);
            }
        };
        echo "<script>alert('Berhasil!');history.go(-1);</script>";
    }

    public function nilai()
    {
        $data = array(
            'nilai'   => $this->input->post('nilai'),
            'review'   =>  $this->input->post('review'),
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => $this->session->userdata('lppmp_session')['userid']
        );

        // notifikasi ke ketua usulan jika setuju
        $this->_createNotif($this->input->post('kd_usulan'), $this->input->post('lppmp'), ' telah menilai usulan', 1);
        // menerima/menolak ajakan sebagai anggota
        $this->model_crud->updatedata('tbl_reviewer_hasil', 'id', $this->input->post('id_hasil'), $data);
        echo "<script>alert('Berhasil!');history.go(-1);</script>";
    }

    private function _createNotif($kd, $to, $content, $jns)
    {
        $notif = array(
            'jns_notif'  => $jns,
            'kd_usulan'  => $kd,
            'to'         => $to,
            'from'       => $this->session->userdata('lppmp_session')['userid'],
            'content'    => $this->session->userdata('lppmp_session')['name'] . $content,
            'is_read'    => 0,
            "created_at" => date('Y-m-d H:i:s'),
            "created_by" => $this->session->userdata('lppmp_session')['userid'],
            "is_deleted" => 0
        );
        // membuat notifikasi ke fakultas atau lppmp
        $this->model_crud->insertdata('tbl_notification', $notif);
    }
    private function _createLogstatus($usulan, $kd_usulan, $status, $current, $next, $note)
    {
        $log = array(
            'jenis_usulan' => $usulan,
            'kd_usulan'    => $kd_usulan,
            'status'       => $status,
            "current_desc" => $current,
            "next_desc"    => $next,
            "note"         => $note,
            "created_at"   => date('Y-m-d H:i:s'),
            "created_by"   => $this->session->userdata('lppmp_session')['userid'],
            "is_deleted"   => 0
        );
        // menyimpan riwayat status pengajuan
        $this->model_crud->insertdata('tbl_status_log', $log);
    }
}
