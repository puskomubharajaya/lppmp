<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_forms extends CI_Model
{

    function getDatabyFak($tbl, $key, $sort)
    {
        $hasil = $this->db->select('*')
            ->from($tbl)
            ->where('is_deleted', 0)
            ->where('fakultas', $this->session->userdata('lppmp_session')['userid'])
            ->group_start()
            ->where('status', 0)
            ->or_where('status', 2)
            ->group_end()
            ->order_by($key, $sort)
            ->get();
        if ($hasil->num_rows() > 0) {
            return $hasil->result();
        } else {
            return array();
        }
    }
    function getDataforRektor($tbl, $key, $sort)
    {
        $hasil = $this->db->select('*')
            ->from($tbl)
            ->where('is_deleted', 0)
            ->group_start()
            ->where('status', 11)
            ->or_where('status', 21)
            ->group_end()
            ->order_by($key, $sort)
            ->get();
        if ($hasil->num_rows() > 0) {
            return $hasil->result();
        } else {
            return array();
        }
    }
    function getDatabyrev($tbl, $nid, $key, $sort)
    {
        $hasil = $this->db->select($tbl . '.id,' .$tbl . '.nid,' .$tbl .'.nidn,' . $tbl . '.fakultas,'.$tbl . '.nama,'.$tbl . '.judul,'.$tbl . '.proposal,'.$tbl . '.status,'.$tbl . '.kd_usulan,tbl_reviewer_hasil.id as id_value')
            ->from($tbl)
            ->where($tbl . '.is_deleted', 0)
            ->join('tbl_reviewer_hasil', 'tbl_reviewer_hasil.kd_usulan = ' . $tbl . '.kd_usulan')
            ->where('tbl_reviewer_hasil.nid', $nid)
            ->group_start()
            ->where($tbl . '.status', 1)
            ->or_where($tbl . '.status', 12)
            ->group_end()
            ->order_by($tbl . '.' . $key, $sort)
            ->get();
        if ($hasil->num_rows() > 0) {
            return $hasil->result();
        } else {
            return array();
        }
    }
    function getNotdeal($kd)
    {
        $sql = $this->db->select('*')
            ->from('tbl_anggota')
            ->where('kd_usulan', $kd)
            ->where('is_deleted', 0)
            ->where('approval !=', 1)
            ->order_by('id', 'DESC')
            ->get();
        return $sql;
    }
    function getNotdeal_reviewer($kd)
    {
        $sql = $this->db->select('*')
            ->from('tbl_reviewer_hasil')
            ->where('kd_usulan', $kd)
            ->where('is_deleted', 0)
            ->where('approval !=', 1)
            ->order_by('id', 'DESC')
            ->get();
        return $sql;
    }
    function getNullvalue($kd)
    {
        $sql = $this->db->select('*')
            ->from('tbl_reviewer_hasil')
            ->where('kd_usulan', $kd)
            ->where('is_deleted', 0)
            ->where('approval', 1)
            ->where('nilai', NULL)
            ->order_by('id', 'DESC')
            ->get();
        return $sql;
    }
}
/* End of file Crud_model.php */
/* Location: ./application/models/Crud_model.php */
