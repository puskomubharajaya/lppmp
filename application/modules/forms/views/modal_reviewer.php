<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="gridSystemModalLabel">Daftar Anggota</h4>
</div>
<style>
    .form-horizontal .has-feedback .form-control-feedback {
        right: 60px;
    }
</style>
<form class="form-horizontal" action="<?php echo base_url(); ?>forms/ubahreviewer" method="post" onsubmit="document.getElementById('submitubah').disabled=true;">
    <div class="modal-body">
        <div class="row">
            <div class="col-sm-12">
                <?php $i = 0;
                foreach ($reviewer->result() as $key) { ?>
                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-2 col-xs-12"><?php echo $key->reviewer ?> : </label>
                        <div class="col-md-10 col-sm-10 col-xs-12 form-group has-feedback">
                            <?php if ($key->approval == 1) {
                                $icon = '<span style="color:green" class="fa fa-check form-control-feedback right">diterima</span>';
                                $disabled = 'disabled';
                            } elseif ($key->approval == 2) {
                                $icon = '<span style="color:red" class="fa fa-remove form-control-feedback right">ditolak</span>';
                                $disabled = '';
                            } else {
                                $icon = '<span class="fa fa-spinner form-control-feedback right">menunggu</span>';
                                $disabled = 'disabled';
                            } ?>
                            <select class="form-control" id="reviewer" name="reviewer[<?php echo $i ?>]" required <?php echo $disabled ?>>
                                <?php foreach ($allreviewer as $row) { ?>
                                    <option value="<?php echo $row->nid . '-' . $row->nama ?>" <?php echo ($row->nid . '-' . $row->nama == $key->nid . '-' . $key->nama) ? 'selected' : '' ?>><?php echo $row->nid . ' - ' . $row->nama ?></option>
                                <?php } ?>
                            </select><?php echo $icon ?>
                            <input type="hidden" name="id_reviewer[<?php echo $i ?>]" value="<?php echo $key->id ?>" <?php echo $disabled ?>>
                            <input type="hidden" name="kd_usulan" value="<?php echo $key->kd_usulan ?>">
                        </div>
                    </div>
                <?php $i++;
                } ?>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <?php
        if ($notDeal->num_rows() > 0) { ?>
            <button type="submit" class="btn btn-primary" id="submitubah">Ubah</button>
        <?php } ?>
    </div>
</form>

<script>
    for (let index = 0; index < 2; index++) {
        $("#anggota" + index).autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: '<?php echo base_url('proposal/load_dosen_autocomplete'); ?>',
                    data: {
                        term: request.term
                    },
                    dataType: "json",
                    type: "POST",
                    success: function(data) {
                        response(data);
                    }
                });
            },
            select: function(evt, ui) {
                $('#anggota' + index).val(ui.item.value);
                $('#nama' + index).val(ui.item.name);
                $('#nid' + index).val(ui.item.save);
            }
        });
    }
</script>