<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
    </button>
    <h4 class="modal-title" id="myModalLabel2">Pengajuan <?php echo getUsulan($usulan) ?></h4>
</div>
<form id="plot" class="form-horizontal" method="POST" action="<?php echo base_url(); ?>forms/nilai" onsubmit="document.getElementById('submitnilai').disabled=true;">
    <input type="hidden" value="<?php echo $usulan ?>" name="usulan">
    <input type="hidden" value="<?php echo $proposal->kd_usulan ?>" name="kd_usulan">
    <input type="hidden" value="<?php echo $reviewer->id ?>" name="id_hasil">
    <input type="hidden" value="<?php echo $reviewer->created_by ?>" name="lppmp">
    <div class="modal-body">
        <div class="row">
            <div class="col-sm-6">
                <table width="100%">
                    <tr>
                        <th width="15%">NID</th>
                        <th width="3%">:</th>
                        <td><?php echo $proposal->nid ?></td>
                    </tr>
                    <tr>
                        <th>NIDN</th>
                        <th>:</th>
                        <td><?php echo $proposal->nidn ?></td>
                    </tr>
                    <tr>
                        <th>Nama</th>
                        <th>:</th>
                        <td><?php echo $proposal->nama ?></td>
                    </tr>
                    <tr>
                        <th>Telp</th>
                        <th>:</th>
                        <td><?php echo $proposal->tlp ?></td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <th>:</th>
                        <td><?php echo $proposal->email ?></td>
                    </tr>
                    <tr>
                        <th>Fakultas</th>
                        <th>:</th>
                        <td><?php echo getFakultasname($proposal->fakultas) ?></td>
                    </tr>
                </table>
            </div>
            <div class="col-sm-6">
                <table width="100%">
                    <tr>
                        <th width="15%">Jenis</th>
                        <th width="3%">:</th>
                        <td><?php echo getJenis($proposal->jenis) ?></td>
                    </tr>
                    <tr>
                        <th>Tahun</th>
                        <th>:</th>
                        <td><?php echo $proposal->tahun ?></td>
                    </tr>
                    <tr>
                        <th>Judul</th>
                        <th>:</th>
                        <td>
                            <?php echo $proposal->judul ?>
                            <a data-toggle="tooltip" data-placement="top" title="Cek Proposal" class="btn btn-info btn-xs" href="<?php echo $proposal->proposal ?>" target="_blank">
                                <i class="fa fa-file-pdf-o"></i>
                            </a>
                            <a data-toggle="tooltip" data-placement="right" title="Cek Plagiarism" class="btn btn-info btn-xs" href="<?php echo $proposal->plagiarism ?>" target="_blank">
                                <i class="fa fa-copy"></i>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <th>JabFung</th>
                        <th>:</th>
                        <td><?php echo getJabfung($proposal->jabfung) ?></td>
                    </tr>
                    <tr>
                        <th>Anggota</th>
                        <th>:</th>
                        <td><?php echo $anggota->num_rows() ?></td>
                    </tr>
                    <tr>
                        <th>Dana</th>
                        <th>:</th>
                        <td>Rp.<?php echo number_format($proposal->dana, 0, ",", "."); ?></td>
                    </tr>
                </table>
            </div>
        </div>
        <?php if ($anggota->num_rows > 0) { ?>
            <br>
            <table class="table">
                <?php $i = 1;
                foreach ($anggota->result() as $key) { ?>
                    <tr>
                        <th><?php echo ucfirst($key->anggota) ?></th>
                        <th>:</th>
                        <td><?php echo $key->nid . ' - ' . $key->nama ?></td>
                    </tr>
                <?php $i++;
                } ?>
            </table>
        <?php } ?>
        <div class="form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12">Nilai</label>
            <div class="col-md-10 col-sm-10 col-xs-12">
                <input type="text" class="form-control" onkeypress="return numeric(event, false)" name="nilai" required maxlength="5" />
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12">Catatan</label>
            <div class="col-md-10 col-sm-10 col-xs-12">
                <textarea name="review" class="form-control" rows="3" required></textarea>
                </select>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
        <button type="submit" class="btn btn-primary" id="submitnilai">Simpan</button>
    </div>
</form>