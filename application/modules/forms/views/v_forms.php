<script type="text/javascript">
    function approve(usulan, id) {
        $("#isi_approve").load('<?php echo base_url() ?>forms/approve/' + usulan + '/' + id);
    }

    function ploting(usulan, id) {
        $("#isi_plot").load('<?php echo base_url() ?>forms/plot_reviewer/' + usulan + '/' + id);
    }

    function lihatreviewer(usulan, id) {
        $("#isi_reviewer").load('<?php echo base_url() ?>forms/lihat_reviewer/' + usulan + '/' + id);
    }

    function nilaireview(usulan, id) {
        $("#isi_nilai").load('<?php echo base_url() ?>forms/nilai_reviewer/' + usulan + '/' + id);
    }

    function app_usulan(usulan, id) {
        $("#isi_app_usulan").load('<?php echo base_url() ?>forms/approve_lppmp/' + usulan + '/' + id);
    }
</script>
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>Form <small>Pengajuan</small></h3>
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Daftar<small>Pengajuan usulan <?php echo getUsulan($proposal) ?></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>NID</th>
                                <th>NIDN</th>
                                <th>Fakultas</th>
                                <th>Nama</th>
                                <th>Judul</th>
                                <th>Status</th>
                                <?php if ($this->session->userdata('lppmp_session')['group'] == 25) { ?>
                                    <th>Reviewer</th>
                                <?php } ?>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            foreach ($usulan as $key) {
                            ?>
                                <tr>
                                    <td><?php echo $i ?></td>
                                    <td><?php echo $key->nid ?></td>
                                    <td><?php echo $key->nidn ?></td>
                                    <td><?php echo getFakultasname($key->fakultas) ?></td>
                                    <td><?php echo $key->nama ?></td>
                                    <td><a href="<?php echo $key->proposal ?>"><?php echo $key->judul ?></a></td>
                                    <td><?php echo getStatus($key->status) ?></td>
                                    <?php if ($this->session->userdata('lppmp_session')['group'] == 25) {
                                        $reviewer = $this->model_crud->getdetail('tbl_reviewer_hasil', 'kd_usulan', $key->kd_usulan, 'id', 'asc');
                                    ?>
                                        <td>
                                            <?php if ($reviewer->num_rows() > 0) { ?>
                                                <ol>
                                                    <?php foreach ($reviewer->result() as $row) { ?>
                                                        <li><?php echo $row->nama ?>
                                                            <i <?php if ($row->approval == 1) {
                                                                    echo 'style="color:green" class="fa fa-check"';
                                                                } else if ($row->approval == 2) {
                                                                    echo 'style="color:red" class="fa fa-remove"';
                                                                } else {
                                                                    echo 'class="fa fa-spinning"';
                                                                } ?>></i>
                                                        </li>
                                                    <?php } ?>
                                                </ol>
                                            <?php } else {
                                                echo '-';
                                            } ?>
                                        </td>
                                    <?php } ?>
                                    <td>
                                        <?php
                                        $arr = array(
                                            'kd_usulan'  => $key->kd_usulan,
                                            'nid'        => $this->session->userdata('lppmp_session')['userid'],
                                            'is_deleted' => 0
                                        );

                                        // tombol untuk reviewer
                                        if ($user == 'reviewer') {
                                            $approval = $this->model_crud->getMoreWhere('tbl_reviewer_hasil', $arr, 'id', 'desc')->row();
                                            if ($approval->approval == 0) {
                                        ?>
                                                <a href="<?php echo base_url() ?>forms/approve_reviewer/<?php echo $key->id_value ?>/yes" class="btn btn-success btn-sm">
                                                    <i class="fa fa-check" title="Terima"></i>
                                                </a>
                                                <a href="<?php echo base_url() ?>forms/approve_reviewer/<?php echo $key->id_value ?>/no" class="btn btn-danger btn-sm">
                                                    <i class="fa fa-remove" title="Tolak"></i>
                                                </a>
                                            <?php
                                            } else if ($approval->approval == 1 && is_null($approval->nilai)) {
                                            ?>
                                                <a class="btn btn-success btn-sm" onclick="nilaireview('<?php echo $proposal ?>',<?php echo $key->id ?>)" data-toggle="modal" href="#nilai">
                                                    <i class="fa fa-pencil-square-o"></i>
                                                </a>
                                            <?php
                                            } else if ($approval->approval == 2) {
                                                echo 'tidak setuju';
                                            }
                                        }

                                        // tombol untuk fakutas
                                        if ($key->status == 0 && $user == 'fakultas') {
                                            ?>
                                            <a class="btn btn-info btn-sm" onclick="approve('<?php echo $proposal ?>',<?php echo $key->id ?>)" data-toggle="modal" href="#approve">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <?php
                                        }

                                        // tombol untuk lppmp
                                        if ($key->status == 1  && $this->session->userdata('lppmp_session')['group'] == 25) {
                                            if ($reviewer->num_rows() == 0) {
                                            ?>
                                                <a class="btn btn-info btn-sm" onclick="ploting('<?php echo $proposal ?>',<?php echo $key->id ?>)" data-toggle="modal" href="#ploting" title="tambah reviewer">
                                                    <i class="fa fa-user"></i>
                                                </a>
                                            <?php
                                            } else {
                                            ?>
                                                <a class="btn btn-warning btn-sm" onclick="lihatreviewer('<?php echo $proposal ?>',<?php echo $key->id ?>)" data-toggle="modal" href="#lihat" title="lihat reviewer">
                                                    <i class="fa fa-user"></i>
                                                </a>
                                                <?php
                                                $nullValue = $this->model_forms->getNullvalue($key->kd_usulan);
                                                if ($nullValue->num_rows() == 0) {
                                                ?>
                                                    <a class="btn btn-info btn-sm" onclick="app_usulan('<?php echo $proposal ?>',<?php echo $key->id ?>)" data-toggle="modal" href="#app_usulan" title="terima/tolak usulan">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                            <?php
                                                }
                                            }
                                        }

                                        // tombol untuk rektor
                                        if ($key->status == 11 && $user == 'rektor') {
                                            ?>
                                            <a href="<?php echo base_url() ?>prints/pengajuan/<?php echo $key->id ?>/<?php echo $proposal ?>" class="btn btn-info btn-sm">
                                                <i class="fa fa-eye" title="Lihat"></i>
                                            </a>
                                            <a class="btn btn-success btn-sm" onclick="app_usulan('<?php echo $proposal ?>',<?php echo $key->id ?>)" data-toggle="modal" href="#app_usulan" title="terima/tolak usulan">
                                                <i class="fa fa-check"></i>
                                            </a>
                                        <?php
                                        }
                                        ?>
                                    </td>
                                </tr>
                            <?php $i++;
                            } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="approve" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" id="isi_approve">
        </div>
    </div>
</div>
<div class="modal fade" id="ploting" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" id="isi_plot">
        </div>
    </div>
</div>
<div class="modal fade" id="lihat" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" id="isi_reviewer">
        </div>
    </div>
</div>
<div class="modal fade" id="nilai" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" id="isi_nilai">
        </div>
    </div>
</div>
<div class="modal fade" id="app_usulan" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" id="isi_app_usulan">
        </div>
    </div>
</div>