<?php defined('BASEPATH') or exit('No direct script access allowed');

class Kontak extends CI_Controller
{

    public function index()
    {
        $data['page'] = 'v_kontak';
        $this->load->view('template/homepage', $data);
    }

    public function send_mail()
    {
        $from_email     =  $this->input->post('email');
        $to_email       =  "lsp@ubharajaya.ac.id";

        $config  =  array(
            'protocol'  => 'smtp',
            'smtp_host' => 'ssl://smtp-relay.gmail.com',
            'smtp_port' => 465,
            'smtp_user' => $to_email,
            'smtp_pass' => 'Ubharaj4y4',
            'mailtype'  => 'html',
            'charset'   => 'iso-8859-1'
        );

        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($from_email, $this->input->post('name'));
        $this->email->to($to_email);
        $this->email->subject($this->input->post('subject'));
        $this->email->message($this->input->post('message'));

        if ($this->email->send()) {
            $this->session->set_flashdata("notif", "Email berhasil terkirim.");
        } else {
            $this->session->set_flashdata("notif", "Email gagal dikirim.");
        }
        redirect('kontak', 'refresh');
    }
}
