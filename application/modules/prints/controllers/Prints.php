<?php defined('BASEPATH') or exit('No direct script access allowed');

class Prints extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    public function pengajuan($id,$usulan)
    {
        $mpdf             = new \Mpdf\Mpdf(['format' => 'A4', 'margin_top' => '10',]);
        $proposal         = $this->model_crud->getDetail(getTblusulan($usulan), 'id', $id, 'id', 'ASC')->row();
        $data['proposal'] = $proposal;
        $data['usulan']   = $usulan;
        $page             = $this->load->view('print_surat', $data, TRUE);
        $mpdf->SetHTMLFooter('<hr>Lembaga Penelitian Pengabdian Kepada Masyarakat dan Publikasi ' . date('Y') . ' <div style="text-align: right;">{PAGENO}<div>');
        $mpdf->WriteHTML($page);
        $mpdf->Output('surat_pengajuan.pdf', 'I');
    }
    
    public function validation($id, $npm)
    {
        $arr = array(
            'id'  => $id,
            'npm' => $npm
        );
        $apl01         = $this->model_crud->getMoreWhere('tbl_apl01', $arr)->row();
        $data['npm']   = $npm;
        $data['apl01'] = $apl01;
        if (!$apl01) {
            // kembali ke halaman akun
            echo '<script>alert("Data yang anda cari tidak ditemukan");window.location.href="' . base_url() . '";</script>';
        }

        $this->load->view('v_validation', $data);
    }
    public function admin_validation($id)
    {
        $apl01         = $this->model_crud->getDetail('tbl_apl01', 'id',$id, 'id', 'ASC')->row();
        $data['apl01'] = $apl01;
        if (!$apl01) {
            // kembali ke halaman akun
            echo '<script>alert("Data yang anda cari tidak ditemukan");window.location.href="' . base_url() . '";</script>';
        }

        $this->load->view('v_validation_admin', $data);
    }
}
