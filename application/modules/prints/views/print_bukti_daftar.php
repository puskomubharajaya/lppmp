<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bukti Daftar-<?php echo $apl01->npm ?></title>
</head>
<style>
    .table_print {
        overflow: wrap;
        width: 100%;
    }

    .footer {
        position: fixed;
        bottom: 5cm;
    }

    .table_print tr th,
    .table_print tr td {
        padding: 8px;
    }
</style>

<body style="font-size: 11vw;">

    <table style="width: 100%;">
        <tr>
            <th><img src="<?php echo base_url() ?>uploads/logo/lsp.png" alt="lsp" style="height: 70px;"></th>
            <th>
                <h2>TANDA BUKTI PENDAFTARAN UJI KOMPETENSI SERTIFIKASI<br>LEMBAGA SERTIFIKASI PROFESI UBHARA JAYA</h2>
            </th>
        </tr>
    </table>
    <hr style="margin-bottom:0px">
    <hr style="margin-top:1px">
    <table class="table_print">
        <tr>
            <th style="text-align: left;width:20%">No. Registrasi</th>
            <th style="width:5%">:</th>
            <td><?php echo $apl01->no_regist ?></td>
        </tr>
        <tr>
            <th style="text-align: left;width:20%">Nama</th>
            <th style="width:5%">:</th>
            <td><?php echo $apl01->name ?></td>
        </tr>
        <tr>
            <th style="text-align: left;width:20%">NIK</th>
            <th style="width:5%">:</th>
            <td><?php echo $apl01->nik ?></td>
        </tr>
        <tr>
            <th style="text-align: left;width:20%">Program Studi</th>
            <th style="width:5%">:</th>
            <td><?php echo getProdi($apl01->prodi) ?></td>
        </tr>
        <tr>
            <th style="text-align: left;width:20%">Skema</th>
            <th style="width:5%">:</th>
            <td><?php echo getSkema($apl01->id_skema) ?></td>
        </tr>
        <tr>
            <th style="text-align: left;width:20%">NPM</th>
            <th style="width:5%">:</th>
            <td><?php echo $apl01->npm ?></td>
        </tr>
    </table>
    <div class="footer">
        <table class="table_print">
            <tr>
                <th style="width: 40%;">ASESI</th>
                <th style="width: 20%;"></th>
                <th style="width: 40%;">PETUGAS LSP</th>
            </tr>
            <tr>
                <th style="width: 40%;">
                    <?php if ($qrcode && $qrcode->asesi) { ?>
                        <img src="<?php echo base_url() ?>/uploads/qrcode/<?php echo $apl01->id ?>/<?php echo $qrcode->asesi ?>" width="100px" alt="qrcode asesi">
                    <?php } ?>
                </th>
                <th style="width: 20%;"></th>
                <th style="width: 40%;">
                    <?php if ($qrcode && $qrcode->petugas) { ?>
                        <img src="<?php echo base_url() ?>/uploads/qrcode/<?php echo $apl01->id ?>/<?php echo $qrcode->petugas ?>" width="100px" alt="qrcode petugas">
                    <?php } ?>
                </th>
            </tr>
            <tr>
                <th style="width: 40%;">(<?php echo $apl01->name ?>)</th>
                <th style="width: 20%;"></th>
                <th style="width: 40%;">(<?php echo $apl01->reviewed_by ?>)</th>
            </tr>
        </table>
    </div>
</body>

</html>