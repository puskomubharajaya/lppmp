<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>APL02-<?php echo $apl01->npm ?></title>
</head>
<style>
    .logo {
        font-family: fontawesome;
    }

    .table_print tr th,
    .table_print tr td {
        padding: 5px;
    }

    .tbls {
        border-collapse: collapse;
        overflow: wrap;
        width: 100%;
    }

    .tbls tr th,
    .tbls tr td {
        border: 0px solid white;
        padding: 5px;
    }

    .tbla {
        border-collapse: collapse;
        overflow: wrap;
        width: 100%;
    }

    .tbla tr th,
    .tbla tr td {
        border: 1px solid black;
        padding: 5px;
    }
</style>

<body style="font-size: 11vw;">
    <img src="<?php echo base_url() ?>uploads/logo/lsp.png" alt="lsp" style="height: 30px;opacity: 0.5;">
    <img src="<?php echo base_url() ?>uploads/logo/bnsp.png" alt="bnsp" style="height: 30px;opacity: 0.5;">
    <h3><b>FR.APL-02 ASESMEN MANDIRI</b></h3>
    <h3>Nama Calon peserta Uji : <?php echo $apl01->name ?></h3>
    <table style="border-collapse: collapse;overflow: wrap;width:100%" border="1" class="tbla">
        <tr>
            <td colspan="2">
                <h4><b>PANDUAN ASESMEN MANDIRI</b></h4>
            </td>
        </tr>
        <tr>
            <td width="15%">
                <h4>Nama Skema</h4>
            </td>
            <td>
                <h4><?php echo getSkema($apl01->id_skema) ?><br><?php echo getNoskema($apl01->id_skema) ?></h4>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <h4><b>Instruksi:</b>
                    <ol>
                        <li>Baca dan pahami setiap pertanyaan di kolom sebelah kiri dari setiap Elemen kopetensi.</li>
                        <li>Tandai pada kotak jika Anda yakin dapat melakukan tugas yang dijelaskan.</li>
                        <li>Isi kolom di sebelah kanan dengan daftar bukti yang Anda miliki untuk menunjukkan bahwa Anda kompeten dalam melakukan tugas-tugas ini. Lampiran dokumen bukti sesuai Persyaratan Dasar pada APL-01.</li>
                    </ol>
                </h4>
            </td>
        </tr>
    </table>

    <table class="tbla">
        <tr>
            <th style="vertical-align: middle;">
                Dapatkah Saya?<br>
                <small>(Pengakuan diri dari calon peserta bahwa dirinya sudah kompeten)</small>
            </th>
            <th width="10%" style="vertical-align: middle;">
                K
            </th>
            <th width="10%" style="vertical-align: middle;">
                BK
            </th>
            <th width="30%" style="vertical-align: middle;">
                Bukti<br>
                <small>(Berdasarkan lampiran bukti)</small>
            </th>
        </tr>
        <?php
        $i = 1;
        foreach ($apl02 as $key) { ?>
            <tr>
                <td>
                    <h3>
                        <b>
                            <?php echo $i; ?>. Judul Unit:
                        </b>
                    </h3>
                    <p><?php echo getUnit($key->id_unit) ?><br>No Unit: <?php echo getKdunit($key->id_unit) ?></p>
                    <br>
                    <div>&bull;&nbsp;Elemen:</div>
                    <table class="tbls">
                        <?php $sub = 1;
                        $elemen = $this->model_crud->getDetail('tbl_elemen', 'id_unit', $key->id_unit, 'id', 'ASC')->result();
                        foreach ($elemen as $row) { ?>
                            <tr>
                                <td><?php echo $i . '.' . $sub . '&nbsp;'  ?></td>
                                <td><?php echo $row->elemen ?></td>
                            </tr>
                        <?php $sub++;
                        } ?>
                    </table>
                </td>
                <td style="vertical-align: middle; text-align:center;">
                    <?php echo ($key->jawaban == 'k') ? '<span class="logo">&#xf046;</span>' : '<span class="logo">&#xf096;</span>' ?>
                </td>
                <td style="vertical-align: middle; text-align:center;">
                    <?php echo ($key->jawaban == 'bk') ? '<span class="logo">&#xf046;</span>' : '<span class="logo">&#xf096;</span>' ?>
                </td>
                <td style="vertical-align: middle; text-align:center;">
                    <a target="_blank" href="<?php echo base_url() . getImage($key->bukti) ?>"><?php echo getNameimg($key->bukti) ?></a>
                </td>
            </tr>
        <?php $i++;
        } ?>
    </table>
    <br>
    <table class="tbla">
        <tr>
            <td style="vertical-align: right-top;">Nama Calon Peserta Uji: <br><?php echo $apl01->name ?></td>
            <td style="vertical-align: right-top;" colspan="2">Tanggal: <br><?php echo datetimeNotime($apl01->created_at) ?></td>
            <td style="vertical-align: right-top;">
                Tanda Tangan Asesi: <br>
                <?php if ($qrcode && $qrcode->asesi) { ?>
                    <img src="<?php echo base_url() ?>/uploads/qrcode/<?php echo $apl01->id ?>/<?php echo $qrcode->asesi ?>" width="100px" alt="qrcode asesi">
                <?php } ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <b>Ditinjau oleh Staf Admin LSP dan / atau Asesor</b>
            </td>
        </tr>
        <tr>
            <td rowspan="2" style="vertical-align: right-top;">Nama Admin LSP / Asesor: <br></td>
            <td colspan="2">Rekomendasi:</td>
            <td rowspan="2" style="vertical-align: right-top;">Tanda Tangan dan Tanggal:</td>
        </tr>
        <tr>
            <td>
                <span class="logo">&#xf096;</span>Dilanjutkan<br>
                <span class="logo">&#xf096;</span>Tidak dilanjutkan
            </td>
            <td>
                <span class="logo">&#xf096;</span>Portofolio<br>
                <span class="logo">&#xf096;</span>Uji Kompetensi
            </td>
        </tr>


    </table>
</body>

</html>