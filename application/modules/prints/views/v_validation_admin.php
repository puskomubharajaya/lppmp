<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Validation</title>
</head>
<style>
    .table_print tr th,
    .table_print tr td {
        padding: 8px;
        border: 0px;
    }

    .mid {
        margin: 250px auto;
        width: 400px;
        padding: 10px;
        border: 1px solid #ccc;
    }

    .table_print {
        width: 100%;
        overflow: wrap;
    }

    .table_print tr .thy {
        width: 2%;
        text-align: left
    }

    .table_print tr .thx {
        width: 15%;
        text-align: left
    }

    .stat {
        color: green;
        font-weight: bold;
        font-size: 150%;
    }
</style>

<body>
    <div class="mid">
        <table class="table_print">
            <tr>
                <th class="thx">
                    No. Registrasi
                </th>
                <th class="thy">
                    :
                </th>
                <td>
                    <?php echo $apl01->no_regist ?>
                </td>
            </tr>
            <tr>
                <th class="thx">
                    Nama
                </th>
                <th class="thy">
                    :
                </th>
                <td>
                    <?php echo $apl01->reviewed_by ?>
                </td>
            </tr>
            <tr>
                <th class="thx">
                    Tanggal
                </th>
                <th class="thy">
                    :
                </th>
                <td>
                    <?php echo datetimeNotime($apl01->reviewed_at) ?>
                </td>
            </tr>
            <tr>
                <th class="thx">
                    Keterangan
                </th>
                <th class="thy">
                    :
                </th>
                <td class="stat">
                    Telah Menyetujui Form Asesi
                </td>
            </tr>
        </table>
        <div style="float:right">
            <a href="https://lsp.ubharajaya.ac.id/prints/apl01/<?php echo $apl01->id ?>"><button>APL-01</button></a>
            <a href="https://lsp.ubharajaya.ac.id/prints/apl02/<?php echo $apl01->id ?>"><button>APL-02</button></a>
        </div>
        <br>
    </div>
</body>

</html>