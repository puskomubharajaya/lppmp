<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Surat</title>
</head>
<style>
    .logo {
        font-family: fontawesome;
    }

    .container {
        margin: 0px 50px;
    }
</style>

<body style="font-size: 12vw;">
    <h3 style="margin-bottom: 0;"><b>UNIVERSITAS BHAYANGKARA JAKARTA RAYA</b></h3>
    <h3 style="margin-top: 0;"><u>LEMBAGA PENELITIAN PENGABDIAN KEPADA MASYARAKAT DAN PUBLIKASI</u></h3>
    <br>
    <div class="container">
        <h3 style="text-align: center;"><u>PERMOHONAN PENGAJUAN PENDANAAN</u></h3>
        <table>
            <tr>
                <td>Kepada Yth</td>
                <td>:</td>
                <td>Rektor UBJ</td>
            </tr>
            <tr>
                <td>Dari</td>
                <td>:</td>
                <td>Kepala Lembaga Penelitian Pengabdian Kepada Masyarakat Dan Publikasi</td>
            </tr>
            <tr>
                <td>Perihal</td>
                <td>:</td>
                <td>Pengajuan Pendanaan Untuk Kegiatan <?php echo getUsulan($usulan) ?> Bagi Dosen Yang Telah Dirivew</td>
            </tr>
        </table>
        <hr>

        <p>
            Bersama ini kami sampaikan perihal pengajuan pendanaan <?php echo getUsulan($usulan) ?> bagi dosen Fakultas <?php echo ucfirst(strtolower(getFakultasname($proposal->fakultas))) ?> yang telah dirivew oleh fakultasnya.
        </p>
        <p>
<?php 
$tahun=explode(' / ',$proposal->tahun)
?>
            Adapun ketentuan dan persyaratannya setelah di cek desk proposal, cek plagiarism dan cek proposal revisi oleh Kepala Bidang Penelitian dan Kepala Bidang Pengabdian Kepada Masyarakat maka Dosen dibawah ini berhak diajukan pendanaannya disemester <?php echo $tahun[1].' '.$tahun[0].'-'. ($tahun[0]+1)?>.
            <table width="100%">
                <tr>
                    <td>Nama</td>
                    <td>:</td>
                    <td><?php echo $proposal->nama ?></td>
                </tr>
                <tr>
                    <td>Judul</td>
                    <td>:</td>
                    <td><?php echo $proposal->judul ?></td>
                </tr>
                <tr>
                    <td>Fakultas</td>
                    <td>:</td>
                    <td><?php echo ucfirst(strtolower(getFakultasname($proposal->fakultas))) ?></td>
                </tr>
                <tr>
                    <td>Anggaran</td>
                    <td>:</td>
                    <td>Rp. <?php echo number_format($proposal->dana,0, ",", ".") ?></td>
                </tr>
            </table>
        </p>
        <p>
            Demikian permohonan ini kami sampaikan, mohon menjadi periksa.
        </p>

        <table width="100%">
            <tr>
                <th width="30%"></th>
                <th width="30%"></th>
                <th>Bekasi, Agustus 2020</th>
            </tr>
            <tr>
                <th></th>
                <th></th>
                <th>a.n Ka. LPPMP<br>Ka.Bidang Publikasi dan Inovasi</th>
            </tr>
            <tr>
                <th></th>
                <th></th>
                <th style="height: 70px;"></th>
            </tr>
            <tr>
                <th></th>
                <th></th>
                <th><u>Herlawati S.Si., MM., M.Kom</u><br>NIP : 2001452</th>
            </tr>
        </table>

    </div>
</body>

</html>