<?php defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('lppmp_session')) {
            redirect('/', 'refresh');
        }
    }
    public function index()
    {
        $data['page'] = 'v_dashboard';
        $this->load->view('template/dashboard', $data);
    }
}
