<section id="featured">
	<!-- start slider -->
	<div id="da-slider" class="da-slider">
		<div class="da-slide">
			<h2>Easy management</h2>
			<p>
				Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.
			</p>
			<a href="#" class="da-link">Read more</a>
		</div>
		<div class="da-slide">
			<h2>Revolution</h2>
			<p>
				A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.
			</p>
			<a href="#" class="da-link">Read more</a>
		</div>
		<div class="da-slide">
			<h2>Warm welcome</h2>
			<p>
				When she reached the first hills of the Italic Mountains, she had a last view back on the skyline of her hometown Bookmarksgrove, the headline of Alphabet Village and the subline of her own road, the Line Lane.
			</p>
			<a href="#" class="da-link">Read more</a>
		</div>
		<div class="da-slide">
			<h2>Quality Control</h2>
			<p>
				Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.
			</p>
			<a href="#" class="da-link">Read more</a>
		</div>
		<nav class="da-arrows">
			<span class="da-arrows-prev"></span>
			<span class="da-arrows-next"></span>
		</nav>
	</div>
	<!-- end slider -->
</section>
<section class="callaction">
	<div class="container">
		<div class="row">
			<div class="span12">
				<div class="big-cta">
					<div class="cta-text">
						<h3>Ajukan Usulan <span class="highlight"><strong>Penelitian dan Pengabdian</strong></span> Anda</h3>
					</div>
					<div class="cta floatright">
						<a class="btn btn-large btn-theme btn-rounded" href="#">Pengajuan</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="content">
	<div class="container">
		<!-- Portfolio Projects -->
		<div class="row">
			<div class="span12">
				<h4 class="heading">Blog <strong>Terbaru</strong></h4>
				<div class="row">
					<section id="projects">
					</section>
				</div>
			</div>
		</div>
		<!-- End Portfolio Projects -->
	</div>
</section>
<section id="bottom">
	<div class="container">
		<div class="row">
			<div class="span12">
				<div class="aligncenter">
					<div id="twitter-wrapper">
						<div id="twitter">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>