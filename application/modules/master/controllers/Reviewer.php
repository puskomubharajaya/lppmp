<?php defined('BASEPATH') or exit('No direct script access allowed');

class Reviewer extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (($this->session->userdata('lppmp_session')['usertype'] != 6 && $this->session->userdata('lppmp_session')['group'] != 25) || !$this->session->userdata('lppmp_session')) {
            redirect('/', 'refresh');
        }
    }
    public function index()
    {
        $data['fakultas'] = getFakultas();
        $data['reviewer'] = $this->model_crud->getData('tbl_reviewer', 'id', 'DESC');
        $data['page']   = 'v_master_reviewer';
        $this->load->view('template/dashboard', $data);
    }
    public function update($id)
    {
        $data['reviewer'] = $this->model_crud->getDetail('tbl_reviewer', 'id', $id, 'id', 'DESC')->row();
        $this->load->view('modal_edit_reviewer', $data);
    }
    public function add()
    {
        $data = array(
            'nid'        => $this->input->post('nid'),
            'nidn'       => $this->input->post('nidn'),
            'nama'       => $this->input->post('nama'),
            'no_wa'      => $this->input->post('no_wa'),
            'email'      => $this->input->post('email'),
            'fakultas'   => $this->input->post('fakultas'),
            'jabfung'    => $this->input->post('jabfung'),
            "created_at" => date('Y-m-d H:i:s'),
            "created_by" => $this->session->userdata('lppmp_session')['userid'],
            "is_deleted" => 0
        );
        $this->model_crud->insertdata('tbl_reviewer', $data);
        echo "<script>alert('Berhasil!');history.go(-1);</script>";
    }
    public function edit()
    {
        $id   = $this->input->post('id_reviewer');
        $data = array(
            'no_wa'      => $this->input->post('no_wa'),
            'email'      => $this->input->post('email'),
            "updated_at" => date('Y-m-d H:i:s'),
            "updated_by" => $this->session->userdata('lppmp_session')['userid']
        );

        $this->model_crud->updateData('tbl_reviewer', 'id', $id, $data);
        echo "<script>alert('Berhasil!');history.go(-1);</script>";
    }

    public function delete($id)
    {
        $this->_delete($id, 'tbl_reviewer');
        echo "<script>alert('Berhasil!');history.go(-1);</script>";
    }
    private function _delete($id, $tbl)
    {
        $data = array(
            "updated_at" => date('Y-m-d H:i:s'),
            "updated_by" => $this->session->userdata('lppmp_session')['userid'],
            "is_deleted" => 1
        );

        $this->model_crud->updateData($tbl, 'id', $id, $data);
    }

    function load_dosen_autocomplete()
    {
        $data = getDsn_autocomplete($this->input->post('term', TRUE));
        echo ($data);
    }
}
