<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
    </button>
    <h4 class="modal-title" id="myModalLabel2">Ubah Reviewer</h4>
</div>
<form class="form-horizontal" method="POST" action="<?php echo base_url(); ?>master/reviewer/edit" onsubmit="document.getElementById('submitbutton1').disabled=true;">
    <div class="modal-body">
        <div class="form-group">
            <label class="control-label col-md-4 col-sm-4 col-xs-12">NID/Nama :</label>
            <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text" class="form-control" value="<?php echo $reviewer->nid . ' - ' . $reviewer->nama ?>" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-4 col-sm-4 col-xs-12">NIDN :</label>
            <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text" class="form-control" value="<?php echo $reviewer->nidn ?>" readonly />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-4 col-sm-4 col-xs-12">No Telp :</label>
            <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text" class="form-control" value="<?php echo $reviewer->no_wa ?>" name="no_wa" onkeypress="return numeric(event, false)" required maxlength="15" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-4 col-sm-4 col-xs-12">Email :</label>
            <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="email" class="form-control" value="<?php echo $reviewer->email ?>" name="email" required maxlength="100" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-4 col-sm-4 col-xs-12">Fakultas :</label>
            <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text" class="form-control" value="<?php echo getFakultasname($reviewer->fakultas) ?>" readonly />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-4 col-sm-4 col-xs-12">Jabatan Fungsional :</label>
            <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text" class="form-control" value="<?php echo getJabfung($reviewer->jabfung) ?>" readonly />
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
        <button type="submit" class="btn btn-primary" id="submitbutton1">Simpan</button>
    </div>
</form>