<script type="text/javascript">
    function edit(id) {
        $("#isi_reviewer").load('<?php echo base_url() ?>master/reviewer/update/' + id);
    }
    $(document).ready(function() {
        $(function() {
            $("#nid0").autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: '<?php echo base_url('master/reviewer/load_dosen_autocomplete'); ?>',
                        data: {
                            term: request.term
                        },
                        dataType: "json",
                        type: "POST",
                        success: function(data) {
                            response(data);
                        }
                    });
                },
                select: function(evt, ui) {
                    if (ui.item.jabfung == 'TPD') {
                        $('#nid0').val('');
                        $('#nama').val('');
                        $('#nid').val('');
                        $('#nidn').val('');
                        $('#telp').val('');
                        $('#email').val('');
                        $('#fakultas').val('');
                        $('#fakultasname').val('');
                        $('#jabfung').val('');
                        $('#jabfungname').val('');
                        $('.save').prop('disabled', true);
                        alert('Tenaga pengajar tidak dapat menjadi reviewer');
                    } else {
                        $('#nid0').val(ui.item.value);
                        $('#nama').val(ui.item.name);
                        $('#nid').val(ui.item.save);
                        $('#nidn').val(ui.item.nidn);
                        $('#telp').val(ui.item.telp);
                        $('#email').val(ui.item.email);
                        $('#fakultas').val(ui.item.fakultas);
                        $('#fakultasname').val(ui.item.fakultasname);
                        $('#jabfung').val(ui.item.jabfung);
                        $('#jabfungname').val(ui.item.jabfungname);
                        $('.save').prop('disabled', false);;
                    }
                }
            });
        });
    });
</script>
<style>
    .ui-autocomplete {
        z-index: 2147483647;
    }
</style>
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>Master <small>reviewer</small></h3>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Daftar<small>reviewer</small>&nbsp;
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li data-toggle="tooltip" data-placement="left" title="Tambah reviewer Baru">
                            <a data-toggle="modal" href="#tambah">
                                <i class="fa fa-plus"></i>
                            </a>
                        </li>
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>NID</th>
                                <th>NIDN</th>
                                <th>Nama</th>
                                <th>No Telp</th>
                                <th>Email</th>
                                <th>Fakultas</th>
                                <th>Jabatan Fungsional</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            foreach ($reviewer as $key) {
                            ?>
                                <tr>
                                    <td><?php echo $i ?></td>
                                    <td><?php echo $key->nid ?></td>
                                    <td><?php echo $key->nidn ?></td>
                                    <td><?php echo $key->nama ?></td>
                                    <td><?php echo $key->no_wa ?></td>
                                    <td><?php echo $key->email ?></td>
                                    <td><?php echo getFakultasname($key->fakultas) ?></td>
                                    <td><?php echo getJabfung($key->jabfung) ?></td>
                                    <td>
                                        <span data-toggle="tooltip" data-placement="left" title="Ubah reviewer">
                                            <a class="btn btn-warning btn-sm" onclick="edit(<?php echo $key->id ?>)" data-toggle="modal" href="#edit">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                        </span>
                                        <a onclick="return confirmhapus();" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Hapus reviewer" href="<?php echo base_url('master/reviewer/delete/' . $key->id) ?>"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            <?php $i++;
                            } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel2">Tambah Reviewer Baru</h4>
            </div>
            <form class="form-horizontal" method="POST" action="<?php echo base_url(); ?>master/reviewer/add" onsubmit="document.getElementById('submitbutton').disabled=true;">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12">NID/Nama :</label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <input type="text" class="form-control" id="nid0" />
                            <input type="hidden" class="form-control" id="nid" name="nid" />
                            <input type="hidden" class="form-control" id="nama" name="nama" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12">NIDN :</label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <input type="text" class="form-control" id="nidn" name="nidn" required maxlength="20" readonly />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12">No Telp :</label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <input type="text" class="form-control" id="telp" name="no_wa" onkeypress="return numeric(event, false)" required maxlength="15" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12">Email :</label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <input type="email" class="form-control" id="email" name="email" required maxlength="100" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12">Fakultas :</label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <input type="text" class="form-control" id="fakultasname" required maxlength="100" readonly />
                            <input type="hidden" id="fakultas" name="fakultas" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12">Jabatan Fungsional :</label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <input type="text" class="form-control" id="jabfungname" required maxlength="100" readonly />
                            <input type="hidden" id="jabfung" name="jabfung" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                    <button type="submit" class="btn btn-primary save" id="submitbutton">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="isi_reviewer">
        </div>
    </div>
</div>

