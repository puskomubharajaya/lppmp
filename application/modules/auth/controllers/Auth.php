<?php defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends MX_Controller
{
    // public function index()
    // {
    //     if (!$this->session->userdata('lppmp_session') || is_null($this->session->userdata('lppmp_session'))) {
    //         $is_cookie_exist = $this->_is_has_cookie();
    //         if (($is_cookie_exist === NULL) or ($is_cookie_exist === FALSE)) {
    //             $this->load->view('v_login');
    //         }
    //     } else {
    //         redirect('/', 'refresh');
    //     }
    // }
    public function login()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $body     = json_encode(['username' => $username, 'password' => $password]);
        $exec     = $this->curl_lib->exec_curl($this->hostapi . '/api_lppmp/attemp_login', $this->http_header, 1, $body);

        $this->_decode_auth_response($exec);
    }
    public function logout()
    {
        setcookie('sia_cookie', "", time() - 7200, '/', 'ubharajaya.ac.id');
        unset($_COOKIE['sia_cookie']);
        $this->session->sess_destroy();
        delete_cookie("sia_cookie");
        redirect('/', 'refresh');
    }
    private function _is_has_cookie()
    {
        $cookie = get_cookie('sia_cookie');
        if (isset($cookie) || !is_null($cookie)) {
            $credential = explode('.', base64_decode($cookie));
            $username   = $credential[0];
            $usertype   = $credential[3];
            $usergroup  = $credential[2];
            $body       = json_encode(['username' => $username, 'usergroup' => $usergroup, 'usertype' => $usertype]);
            $req        = $this->curl_lib->exec_curl($this->hostapi . '/api_lppmp/cookie_login', $this->http_header, 1, $body);

            $this->_decode_auth_response($req);
        } else {
            return false;
        }
    }
    private function _decode_auth_response($res)
    {
        $response = json_decode($res);
        if ($response->status == 1) {
            $this->_create_session((array) $response->data);
        } else if ($response->status == 3) {
            echo '<script>alert("Login fail! Invalid username or password");window.location.href="' . base_url('/') . '";</script>';
        } else {
            delete_cookie('sia_cookie');
            echo '<script>alert("Login fail! You don\'t have permission to access this application.");window.location.href="' . base_url('/') . '";</script>';
        }
    }
    protected function _create_session($data)
    {
        $this->session->set_userdata('lppmp_session', $data);
        $cekReviewer = $this->model_crud->getdetail('tbl_reviewer', 'nid', $data['userid'], 'id', 'desc');

        switch ($data['usertype']) {
            case 1:
                if ($cekReviewer->num_rows() > 0) {
                    return
                        redirect('dashboard', 'refresh');
                    break;
                } else {
                    return
                        redirect('/', 'refresh');
                    break;
                }

            case 6:
                return
                    redirect('dashboard', 'refresh');
                break;

            case 3:
                return
                    redirect('dashboard', 'refresh');
                break;

            default:
                break;
        }
    }
}
