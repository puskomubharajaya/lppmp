<?php defined('BASEPATH') or exit('No direct script access allowed');

class Export extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('lsp_session')['usertype'] != 8 || !$this->session->userdata('lsp_session')) {
            redirect('/', 'refresh');
        }
    }
    public function index()
    {
        $data['apl01'] = $this->model_crud->getDetail('tbl_apl01', 'status', 1, 'id', 'ASC')->result();
        $data['title'] = 'lsp_'.date('YmdHis');
        $this->load->view('export_asesi', $data);
    }
//    https://recodeku.blogspot.com/2016/12/membuat-export-to-excel-pada-codeigniter.html
}
