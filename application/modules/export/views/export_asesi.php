<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=$title.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<style>
    .str {
        mso-number-format: \@;
    }

    .mid {
        text-align: center;
    }

    .vmid {
        vertical-align: middle;
    }

    .midLeft {
        text-align: left;
    }

    .headers {
        background-color: #215967;
        color: #fff
    }
</style>
<table border="1" width="100%">
    <thead>
        <tr>
            <th class="headers">No</th>
            <th class="headers">NAMA ASESI</th>
            <th class="headers">NIK</th>
            <th class="headers">TEMPAT LAHIR</th>
            <th class="headers">TANGGAL LAHIR<br>(dd/mm/yyyy)</th>
            <th class="headers">JENIS KELAMIN<br>(L/P)</th>
            <th class="headers">TEMPAT TINGGAL</th>
            <th class="headers">KODE KAB/KOTA</th>
            <th class="headers">KODE PROVINSI</th>
            <th class="headers">TELP</th>
            <th class="headers">EMAIL</th>
            <th class="headers">KODE PENDIDIKAN</th>
            <th class="headers">KODE PEKERJAAN</th>
            <th class="headers">KODE SKEMA</th>
            <th class="headers">TANGGAL UJI<br>(dd/mm/yyyy)</th>
            <th class="headers">KODE TUK</th>
            <th class="headers">NOMOR REGISTRASI ASESOR</th>
            <th class="headers">SUMBER ANGGARAN</th>
            <th class="headers">INSTANSI PEMBERI ANGGARAN</th>
            <th class="headers">K/BK</th>
        </tr>
    </thead>
    <tbody>
        <?php $i = 1;
        foreach ($apl01 as $key) { ?>
            <tr class="vmid">
                <td class="midLeft"><?php echo $i; ?></td>
                <td class="midLeft"><?php echo $key->name; ?></td>
                <td class="str"><?php echo $key->nik; ?></td>
                <td class="midLeft"><?php echo $key->tp_lahir; ?></td>
                <td class="mid"><?php echo date("d/m/Y", strtotime($key->tg_lahir)); ?></td>
                <td class="mid"><?php echo $key->jenkel; ?></td>
                <td class="mid"><?php echo $key->alamat_rmh; ?></td>
                <td class="str"><?php echo substr($key->nik, 0, 4); ?></td>
                <td class="str"><?php echo substr($key->nik, 0, 2); ?></td>
                <td class="str"><?php echo $key->hp; ?></td>
                <td class="midLeft"><?php echo $key->email; ?></td>
                <td class="mid">7</td>
                <td class="mid">3</td>
                <td class="midLeft"><?php echo getNoskema($key->id_skema); ?></td>
                <td class="mid"></td>
                <td class="midLeft"></td>
                <td class="midLeft"></td>
                <td class="mid">4</td>
                <td class="mid">100</td>
                <td class="mid"></td>
            </tr>
        <?php $i++;
        } ?>
    </tbody>

</table>