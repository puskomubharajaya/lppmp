<?php defined('BASEPATH') or exit('No direct script access allowed');

class Profile extends CI_Controller
{

    public function index()
    {
        $data['page'] = 'v_profile';
        $this->load->view('template/homepage', $data);
    }
}
