<section id="inner-headline">
    <div class="container">
        <div class="row">
            <div class="span12">
                <div class="inner-heading">
                    <h2>Tentang Kami</h2>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="content">
    <div class="container">
        <div class="row">
            <div class="span12">
                <h2>Selamat Datang di <strong>LPPMP</strong></h2>
                <p>
                    LPPMP UBJ merupakan unit yang memberdayakan sumber daya dalam melakukan penelitian inovatif yang mempertimbangkan kearifan lokal serta pengabdian masyarakat yang tepat sasaran.
                </p>
            </div>
        </div>
        <!-- divider -->
        <div class="row">
            <div class="span12">
                <div class="solidline">
                </div>
            </div>
        </div>
        <!-- end divider -->
        <div class="row">
            <div class="span12">
                <h4>Visi</h4>
            </div>
            <div class="span12">
                <p>
                    Menjadi lembaga yang memiliki komitmen tinggi terhadap keunggulan penelitian di bidang sekuriti yang berwawasan kebangsaan di tingkat nasional.
                </p>
            </div>
        </div>
        <!-- divider -->
        <div class="row">
            <div class="span12">
                <div class="solidline">
                </div>
            </div>
        </div>
        <!-- end divider -->
        <div class="row">
            <div class="span12">
                <h4>Misi</h4>
            </div>
            <div class="span12">
                <ol>
                    <li>Mengkoordinasikan, merencanakan, melaksanakan, memantau, dan menilai serta mendokumentasikan pelaksanaan kegiatan penelitian</li>
                    <li>Meningkatkan daya saing dosen dalam kegiatan penelitian di tingkat nasional dan internasional.</li>
                    <li>Meningkatkan kerjasama penelitian di tingkat nasional dan internasional.</li>
                    <li>Mengarahkan kegiatan penelitian yang berorientasi produk (fisik atau nonfisik) dan/atau perolehan Hak Kekayaan Intelektual (HKI).</li>
                    <li>Mendiseminasikan hasil penelitian</li>
                    <li>Membangun sistem informasi hasil penelitian</li>
                    <li>Meningkatkan pemanfaatan hasil penelitian untuk pengembangan proses pembelajaran.</li>
                    <li>Mengembangkan budaya akademik dan kewirausahaan.</li>
                    <li>Meningkatkan keterlibatan mahasiswa dalam kegiatan penelitian</li>
                </ol>
            </div>
        </div>
    </div>
</section>